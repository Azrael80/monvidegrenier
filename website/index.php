<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Page d'accueil avec recherche et affichage des annonces
	*/
	
	require_once("core.php");

	$pageName = "search";

	//page
	$page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
	$announcesPerPage = 12;
	//Nombre de pages
	$pageCount = 1;

	//Préparation des catégories
	//On récupère les catégories avec comme ordre croissant les catégorie parent, afin d'avoir les parents en premier
	$request = Config::db()->query('SELECT * FROM category ORDER BY catId ASC, catParentId ASC');
	$categories = [];
	$catIds = [];

	while($c = $request->fetch(PDO::FETCH_OBJ)) {
		$catIds[] = $c->catId; //Ajoute l'identifiant à la liste des ids de catégorie
		if($c->catParentId == -1)
		{
			//Si la catégorie est un parent, on créer un tableau dans le tableau $categories
			//qui contiendra le nom de la catégorie ainsi qu'un tableau qui contiendra ses fils.
			$categories[$c->catId] = [
				'name' => $c->catName,
				'icon' => $c->catIcon,
				'subCats' => []
			];
		}
		else
		{
			//Si ce n'est pas un parent on l'ajoute alors au catégories fils
			$categories[$c->catParentId]['subCats'][$c->catId] = $c->catName;
		}
	}

	$order = 't_desc';	//Ordre de tri des annonces
	$categoryId = -1;	//Identifiant de la catégorie choisie
	$keywords = ""; //Mots clés
	$localisationText = ""; //Texte pour la localisation
	$localType = -1; //Type de la localisation
	$localId = -1;	//Identifiant de la localisation
	$city = null; //Ville de la recherche
	$dpt = null; //Département de la recherche
	$region = null; //Région de la recherche
	$distance = ""; //Rayon autour de la ville choisie
	$pMin = "";
	$pMax = "";

	//On vérifie que l'ordre d'affichage est correct pour le donner à la variable
	if(isset($_GET['order']) && ($_GET['order'] == 't_desc' || $_GET['order'] == 't_asc' || $_GET['order'] == 'p_desc' || $_GET['order'] == 'p_asc'))
		$order = $_GET['order'];

	if(isset($_GET['category']) && in_array($_GET['category'], $catIds))
		$categoryId = $_GET['category'];

	if(isset($_GET['keywords']))
		$keywords = $_GET['keywords'];

	//Vérifications pour la localisation
	if(isset($_GET['local_type']) && isset($_GET['local_id'])) {

		switch ($_GET['local_type']) {
			//Région
			case 0:
				$request = Config::db()->prepare('SELECT * FROM region WHERE code_region = ?');
				$request->execute([$_GET['local_id']]);
				$region = $request->fetch(PDO::FETCH_OBJ);

				//Le département a été trouver
				if($region != null) {
					$localType = $_GET['local_type'];
					$localId = $_GET['local_id'];
					$localisationText = $region->nom_region;
				}
				break;
			//Département
			case 1:
				$request = Config::db()->prepare('SELECT * FROM departement WHERE departement_id = ?');
				$request->execute([$_GET['local_id']]);
				$dpt = $request->fetch(PDO::FETCH_OBJ);

				//Le département a été trouver
				if($dpt != null) {
					$localType = $_GET['local_type'];
					$localId = $_GET['local_id'];
					$localisationText = $dpt->departement_nom.'('.$dpt->departement_code.')';
				}
				break;
			//Ville
			case 2:
				$request = Config::db()->prepare('SELECT * FROM ville WHERE ville_id = ?');
				$request->execute([$_GET['local_id']]);
				$city = $request->fetch(PDO::FETCH_OBJ);

				//La ville a été trouver
				if($city != null) {
					$localType = $_GET['local_type'];
					$localId = $_GET['local_id'];
					$localisationText = $city->ville_nom_reel.", ".explode('-', $city->ville_code_postal)[0];
				}
				break;
		}

	}

	//Vérification de la distance, elle doit être numérique et plus grande que 0.
	if(isset($_GET['distance']) && is_numeric($_GET['distance']) && $_GET['distance'] > 0)
		$distance = $_GET['distance'];

	//Vérifications sur les prix
	if(isset($_GET['p_min']) && isset($_GET['p_max'])) {
		//Si les deux prix sont entrés et que p_min <= p_max, alors les prix sont valides
		//Remplacement des virgules par des points
		$_GET['p_min'] = str_replace(',', '.', $_GET['p_min']);
		$_GET['p_max'] = str_replace(',', '.', $_GET['p_max']);

		if(!empty($_GET['p_min']) && !empty($_GET['p_max']) && is_numeric($_GET['p_min']) && is_numeric($_GET['p_max']) && $_GET['p_min'] > 0 && $_GET['p_max'] >= $_GET['p_min']) {
			
			//Si les deux prix sont entrés et que p_min <= p_max, alors les prix sont valides
			$pMin = $_GET['p_min'];
			$pMax = $_GET['p_max'];

		} else if(!empty($_GET['p_min']) && empty($_GET['p_max']) && is_numeric($_GET['p_min'])) {

			//Si seulement le prix minimum est entré, on ne prend que celui-ci
			$pMin = $_GET['p_min'];
		} else if(!empty($_GET['p_max']) && empty($_GET['p_min']) && is_numeric($_GET['p_max'])) {

			//Si seulement le prix maximum est entré, on ne prend que celui-ci
			$pMax = $_GET['p_max'];
		}
	}

	//Rercheche
	$announces = Announce::getAnnouncesSearch($page, $announcesPerPage, $categoryId, $keywords, $city, $distance, $dpt, $region, $pMin, $pMax, $order);
	$announcesCount = $announces['full_count'];

	//Calcul du nombre de page
	$pageCount = ceil($announcesCount / $announcesPerPage);

	$announces = $announces['announces'];
	$scriptData['search'] = [
		'order' => $order,
		'catId' => $categoryId
	];
	include("templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
		<div class="container-fluid p-0">
			<div class="jumbotron blue rounded-0 p-4">
				<div class="container">
					<div class="row justify-content-md-center">
						<div class="col-12 col-lg-10">
						  	<div class="card bg-light">
							    <div class="card-body">
							      	<form method="get" id="searchForm">
									  	<div class="form-row">
									  		<div class="form-row col-12">
										  		<div class="input-group mb-3 col-12">
												  	<div class="input-group-prepend">
												    	<button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="modal" data-target="#categoryModal" id="categoryButton"><i class="fa fa-list"></i> TOUTES CATEGORIES</button>
												    	<div class="modal fade" id="categoryModal">
														  	<div class="modal-dialog modal-lg modal-dialog-centered">
																<div class="modal-content">
															  		<div class="modal-header">
																		<h4 class="modal-title" id="categoryModalTitle">CATEGORIES</h4>
																		<button type="button" class="close" data-dismiss="modal" onclick="showAndClose('#connexionBlock', '#forgotBlock', '#connexionModalTitle', 'Connexion');">&times;</button>
																	</div>
																	<div class="modal-body row">
																		<div class="col-12 col-sm-6 col-md-4 col-lg-3">
																			<div role="separator" class="dropdown-divider"></div>
																			<a id="catId-1" class="font-weight-bold" href="#" onclick="chooseCategory(-1);" style="font-size: 0.75rem"><i class="fa fa-list"></i> TOUTES CATEGORIES</a>
																			<div role="separator" class="dropdown-divider"></div>
																		</div>
																		<div class="col-0 col-sm-6 col-md-8 col-lg-9"></div>
																		<?php 
															    		//Affichage des catégories dans le menu.
															    		foreach ($categories as $catParentId => $catParent) {
															    			echo '<div class="col-12 col-sm-6 col-md-4 col-lg-3">';
															    			echo '<div role="separator" class="dropdown-divider"></div>';
															    			echo '<a id="catId'.$catParentId.'" class="font-weight-bold" href="#" onclick="chooseCategory('.$catParentId.');" style="font-size: 0.75rem"><i class="fa fa-'.$catParent['icon'].'"></i> '.$catParent['name'].'</a>';
															    			echo '<div role="separator" class="dropdown-divider"></div>';
															    			foreach ($catParent['subCats'] as $catId => $cat) {
															    				echo '<a id="catId'.$catId.'" href="#" style="color:black;" onclick="chooseCategory('.$catId.');">'.$cat."</a><br />";
															    			}
															    			echo '</div>';
															    		}
															    		?> 
			  														</div>
																</div>
															</div>
														</div>
												  	</div>
												  	<input type="text" name="keywords" class="form-control" placeholder="Mot(s) clés" value="<?= htmlspecialchars($keywords) ?>">
												  	<div class="input-group-append">
    													<span class="input-group-text"><i class="fa fa-search"></i></span>
  													</div>
												</div>
											</div>
									    	<div class="form-row col-12">
										    	<div class="col-6 col-md-4 mb-3" id="search-localisation-typeahead">
										      		<input type="text" class="typeahead form-control col-12" placeholder="Localisation" id="search-localisation" value="<?= $localisationText ?>">
										    	</div>
										    	<div id="localisation-distance" class="input-group mb-3 col-6 col-md-2"<?= $localType != 2 ? ' style="display: none;"' : '' ?>>
										    		<div class="input-group-prepend">
    													<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
  													</div>
  													<input type="text" name="distance" class="form-control" placeholder="Distance km" value="<?= $distance ?>">
										    	</div>
										    	<div class="input-group mb-3 col-6 col-md-2">
										    		<div class="input-group-prepend">
    													<span class="input-group-text"><i class="fa fa-euro-sign"></i></span>
  													</div>
  													<input type="text" name="p_min" class="form-control" placeholder="Prix min" value="<?= $pMin ?>">
										    	</div>
										    	<div class="input-group mb-3 col-6 col-md-2">
  													<input type="text" name="p_max" class="form-control" placeholder="Prix max" value="<?= $pMax ?>">
  													<div class="input-group-append">
    													<span class="input-group-text"><i class="fa fa-euro-sign"></i></span>
  													</div>
										    	</div>
										  	</div>
									  	</div>
									  	<div class="container-fluid text-center">
									  		<input type="hidden" name="category" value="-1">
									  		<input type="hidden" name="local_type" value="<?= $localType ?>">
									  		<input type="hidden" name="local_id" value="<?= $localId ?>">
									  		<input type="hidden" name="order" value="<?= $order ?>">
								  			<button class="btn btn-info" type="submit">Rechercher</button>
								  		</div>
									</form>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row pl-3 pr-3">
				<div class="col-12 p-0">
					<div class="card">
						<div class="card-body font-weight-bold">
						   	<span class="align-middle">Annonces: <?= number_format($announcesCount, 0, '.', ' ') ?></span>
						   	<form class="form-inline float-right">
							    <select id="order" name="order" class="form-control">
							       	<option onclick="changeOrder('t_desc');"<?= ($order == 't_desc' ? 'selected' : '') ?>>Tri : Le plus récent</option>
							        <option onclick="changeOrder('t_asc');"<?= ($order == 't_asc' ? 'selected' : '') ?>>Tri : Le plus ancien</option>
							        <option onclick="changeOrder('p_asc');"<?= ($order == 'p_asc' ? 'selected' : '') ?>>Tri : Prix croissants</option>
							        <option onclick="changeOrder('p_desc');"<?= ($order == 'p_desc' ? 'selected' : '') ?>>Tri : Prix décroissants</option>
							    </select>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 mt-2">
					<nav aria-label="Page navigation example">
					  	<ul class="pagination justify-content-center">
					  		<?php
					  		//Si la page est la première, on affiche pas le bouton précédent
					  		if($page != 1) {
					  		?>
					    	<li class="page-item">
					      		<a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= ($page - 1) ?>" aria-label="Previous">
					        		<span aria-hidden="true">&laquo;</span>
					        		<span class="sr-only">Previous</span>
					      		</a>
					    	</li>
					    	<?php } 

					    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
					    	if($page < 5) {
					    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
					    	?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php 
								}

								//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
								if($pageCount > 5) {
							?>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<li class="page-item"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
							<?php
								}
							} else if($page > $pageCount - 5) {
								//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
								if($pageCount - 5 > 1) {
							?>
							<li class="page-item"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=1">1</a></li>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<?php 
								}
								for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
							?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php
								}
							}

							//Si on est pas sur la dernière page, on affiche le bouton suivant
							if($page != $pageCount) {
					    	?>
					    	<li class="page-item">
					      		<a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= ($page + 1) ?>" aria-label="Next">
					        		<span aria-hidden="true">&raquo;</span>
					        		<span class="sr-only">Next</span>
					      		</a>
					    	</li>
					    	<?php } ?>
					  	</ul>
					</nav>
				</div>
			</div>
			<div class="row justify-content-center" id="announcesContainer">
			<?php 
				foreach($announces as $announce) {
			?> 
				<div class="col-12 col-md-6 col-lg-4 mb-2">
					<a class="text-dark text-left announce-link" href="<?php echo Config::get('site.url'); ?>/announce.php?id=<?= $announce->getId() ?>" title="<?= htmlspecialchars($announce->getTitle()) ?>">
						<div class="card">
							<div class="embed-responsive embed-responsive-16by9">
							<div class="card-img-top embed-responsive-item text-center announce-grey-card">
						  		<img class="announce-card-img" src="<?php echo Config::get('site.url'); ?>/images/<?= $announce->getFirstImage() ?>">
						  	</div>
						  </div>
						  <div class="card-body">
						    	<h6 class="card-title"><?= htmlspecialchars($announce->getTitle()) ?></h6>
						    	<p class="card-text announce-card-desc">
						    		<?php $announce->showCategoryName(); ?><br />
						    		<?php $announce->showCityName(); ?><br />
						    		<?php $announce->showDate(); ?>
						    	</p>
						    	<span class="badge badge-dark font-weight-bold float-left"><i class="fa fa-camera" style="margin-right: 5px;"></i> <?= $announce->getImgCount() ?></span>
						    	<?php 
						    	if($announce->getPrice() > 0) 
						    		echo '<span class="badge badge-dark font-weight-bold float-right">'.number_format($announce->getPrice(), 2, ',', ' ').' <i class="fa fa-euro-sign"></i></span>';
						    	else
						    		echo '<span class="badge badge-dark font-weight-bold float-right">GRATUIT</span>';
						    	?>						    	
						  </div>
						</div>
					</a>
				</div>
			<?php 
				} 
			?>
			</div>
			<div class="row">
				<div class="col-12 mt-2">
					<nav aria-label="Page navigation example">
					  	<ul class="pagination justify-content-center">
					  		<?php
					  		//Si la page est la première, on affiche pas le bouton précédent
					  		if($page != 1) {
					  		?>
					    	<li class="page-item">
					      		<a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= ($page - 1) ?>" aria-label="Previous">
					        		<span aria-hidden="true">&laquo;</span>
					        		<span class="sr-only">Previous</span>
					      		</a>
					    	</li>
					    	<?php } 

					    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
					    	if($page < 5) {
					    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
					    	?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php 
								}

								//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
								if($pageCount > 5) {
							?>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<li class="page-item"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
							<?php
								}
							} else if($page > $pageCount - 5) {
								//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
								if($pageCount - 5 > 1) {
							?>
							<li class="page-item"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=1">1</a></li>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<?php 
								}
								for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
							?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php
								}
							}

							//Si on est pas sur la dernière page, on affiche le bouton suivant
							if($page != $pageCount) {
					    	?>
					    	<li class="page-item">
					      		<a class="page-link" href="?keywords=<?= htmlspecialchars($keywords) ?>&distance=<?= $distance ?>&p_min=<?= $pMin ?>&p_max=<?= $pMax ?>&category=<?= 
							$categoryId ?>&local_type=<?= $localType ?>&local_id=<?= $localId ?>&order=<?= $order ?>&page=<?= ($page + 1) ?>" aria-label="Next">
					        		<span aria-hidden="true">&raquo;</span>
					        		<span class="sr-only">Next</span>
					      		</a>
					    	</li>
					    	<?php } ?>
					  	</ul>
					</nav>
				</div>
			</div>
		</div>
<?php include("templates/footer.php"); ?>