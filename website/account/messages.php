<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
	*/
	
	require_once("../core.php");

	$pageName = "messages";

	if($user == null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	$result = array();

	//Page (10 conversations par page)
	$page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;

	$conversations = $user->getConversations($page);

	//Nombre total de conversation
	$conversationsCount = $conversations[0];

	//On compte le nombre de page (10 annonces par page)
	$pageCount = ceil($conversationsCount/10);

	//suppression du nombre de conversations du tableau (car dans $conversationsCount maintenant)
	unset($conversations[0]);

	//Messages
	$conversation = null;
	$lastDate = "";

	//Lecture d'une conversation
	if(isset($_GET['id'])) {
		$secondUser = User::findById($_GET['id']);
		if($secondUser != null && $secondUser->getId() != $user->getId()) { //On vérifie qu'il ne s'agit pas de son propre compte
			$conversation = new Conversation($user, $secondUser);
			$conversation->loadMessages(); //Chargement des messages
			$conversation->view();
		}
	}

	//Si une conversation existe, on ajoute les variables pour les scripts 
	if($conversation != null) {
		$scriptData['conversation'] = [
			'receiverId' => $conversation->getSecondUser()->getId(),
			'lastId' => $conversation->getLastId(),
			'lastDate' => date('d/m/Y H:i', $conversation->getLastDate())
		];
	}

	include("../templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<div class="container mt-3 mb-3 h-100">
	<div class="card bg-light">
		<div class="card-body">
			<div class="container">
			    <div class="row">
			        <?php include('../templates/account_nav.php'); ?> 
			        <div class="col-lg-8 pl-0 pr-0">
				        <div class="col-12 pl-0 pr-0" style="border:1px solid #dee2e6 !important;">
				        	<div class="row ml-1 mr-1">
				        		<?php 
				        		foreach ($result as $message) { 
				        		?> 
							  	<div class="alert alert-<?php echo $message['type']; ?> alert-dismissible fase show col-12">
							  		<button type="button" class="close" data-dismiss="alert">&times;</button>
				  					<?= $message['content']; ?>
								</div>
							  	<?php } ?> 
				        	</div>
				        	<?php if($conversation != null) { ?>
				        	<div class="row ml-0 mr-0">
				        		<div class="col-12 border-bottom">
				        			<h3><?= htmlspecialchars($conversation->getSecondUser()->getFullName()); ?></h3>
				        		</div>
				        	</div>
				        	<div class="message-box" id="messages-list">
				        		<?php
				        		foreach ($conversation->getMessages() as $message) {
				        			$date = date('d/m/Y H:i', $message['message']->messageDate);

				        			//Si anId est différent de 0, on va chercher l'anonce liée au message
				        			//$announce = ($message['message']->anId != 0 ? Announce::findById($message['message']->anId) : null);

				        			//Affichage de la date des messages
				        			if($date != $lastDate) {
				        		?>
								<div class="row-fluid ml-0 mr-0 font-italic text-center">
									<small><?= $date; ?></small>
								</div>
				        		<?php
				        			}

				        			if($message['user']->getId() != $user->getId()) {
				        				//On vérifie si une annonce est liée, pour afficher le bon format de message
				        				if($message['announce'] == null) {
				        		?>
					            <div class="row">
					            	<div class="col-12 col-md-10 mt-1">
					            		<div class="message blue">
						            		<span class="name"><?= htmlspecialchars($message['user']->getFullName()); ?></span>
											<?= nl2br(htmlspecialchars($message['message']->messageContent)); ?>
										</div>
					            	</div>
					            </div>
					            <?php 
					            		} else {
					            ?>
					            <div class="row">
					            	<div class="col-12 col-md-10 mt-1">
					            		<div class="message blue">
					            			<div class="container-fluid p-1">
					            				<div class="row">
						            				<div class="col-3 text-center">
						            					<a class="text-dark text-left announce-link" href="<?php echo Config::get('site.url'); ?>/announce.php?id=<?= $message['announce']->getId() ?>" title="<?= htmlspecialchars($message['announce']->getTitle()) ?>">
						            					<img class="img-fluid img-thumbnail" src="<?php echo Config::get('site.url'); ?>/images/<?= $message['announce']->getFirstImage() ?>" />
						            					<small><span class="badge badge-dark font-weight-bold float-left d-none d-md-block mt-1"><i class="fa fa-camera" style="margin-right: 5px;"></i> <?= $message['announce']->getImgCount() ?></span></small>
												    	<?php 
												    	if($message['announce']->getPrice() > 0) 
												    		echo '<small><span class="badge badge-dark font-weight-bold float-right mt-1">'.number_format($message['announce']->getPrice(), 2, ',', ' ').' <i class="fa fa-euro-sign"></i></span></small>';
												    	else
												    		echo '<small><span class="badge badge-dark font-weight-bold float-right mt-1">GRATUIT</span></small>';
												    	?>	
						            					</a>
						            				</div>
						            				<div class="col-9 text-left">
						            					<span class="name"><?= htmlspecialchars($message['user']->getFullName()); ?></span>
														<?= nl2br(htmlspecialchars($message['message']->messageContent)); ?>
						            				</div>
						            			</div>
					            			</div>
										</div>
					            	</div>
					            </div>
					            <?php 
					            		}	
					            	} else {
					            		if($message['announce'] == null) {
					            ?>
					            <div class="row">
					            	<div class="col-12 offset-md-2 col-md-10 mt-1">
					            		<div class="message grey float-right">
						            		<span class="name"><?= htmlspecialchars($message['user']->getFullName()); ?></span>
											<?= nl2br(htmlspecialchars($message['message']->messageContent)); ?> 
										</div>
					            	</div>
					            </div>
					            <?php 
					            		} else {
					            ?>
					            <div class="row">
					            	<div class="col-12 offset-md-2 col-md-10 mt-1">
					            		<div class="message grey float-right">
					            			<div class="container-fluid p-1">
					            				<div class="row">
					            					<div class="col-9 text-left">
						            					<span class="name"><?= htmlspecialchars($message['user']->getFullName()); ?></span>
														<?= nl2br(htmlspecialchars($message['message']->messageContent)); ?>
						            				</div>
						            				<div class="col-3 text-center">
						            					<a class="text-dark text-left announce-link" href="<?php echo Config::get('site.url'); ?>/announce.php?id=<?= $message['announce']->getId() ?>" title="<?= htmlspecialchars($message['announce']->getTitle()) ?>">
						            					<img class="img-fluid img-thumbnail" src="<?php echo Config::get('site.url'); ?>/images/<?= $message['announce']->getFirstImage() ?>" />
						            					<small><span class="badge badge-dark font-weight-bold float-left d-none d-md-block mt-1"><i class="fa fa-camera" style="margin-right: 5px;"></i> <?= $message['announce']->getImgCount() ?></span></small>
												    	<?php 
												    	if($message['announce']->getPrice() > 0) 
												    		echo '<small><span class="badge badge-dark font-weight-bold float-right mt-1">'.number_format($message['announce']->getPrice(), 2, ',', ' ').' <i class="fa fa-euro-sign"></i></span></small>';
												    	else
												    		echo '<small><span class="badge badge-dark font-weight-bold float-right mt-1">GRATUIT</span></small>';
												    	?>	
						            					</a>

						            				</div>
						            			</div>
					            			</div>
										</div>
					            	</div>
					            </div>
					            <?php
					            		}
					            	}
					            	$lastDate = $date;
					            }
					            ?>
					        </div>
				            <div class="row ml-0 mr-0">
				        		<div class="col-12 border-top p-1">
				        			<div class="row ml-1 mr-1" id="message-result-box">

				        			</div>
				        			<div class="input-group">
										<textarea class="form-control" aria-label="Message" id="send-message-content"></textarea>
										<div class="input-group-append">
										   	<button class="btn custom btn-primary" type="submit" id="send-message-button">Envoyer</button>
										</div>
									</div>
								</div>
							</div>
							<?php 
								//Aucune conversation chargée
								} else {  
									if(count($conversations) == 0) {
							?>
							<div class="alert alert-dark m-1 text-center font-weight-bold" role="alert">
	  							Aucune conversation pour le moment.
							</div>
							<?php 
									} else { 
							?>
							<table class="table table-striped">
							  	<thead>
							    <tr>
							      	<th class="col-6">Nom</th>
							      	<th>Dernier message</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<?php foreach ($conversations as $conv) { ?>
								<tr class="clickable-row<?= $conv->getUnRead() ? " font-weight-bold" : ""; ?>" data-href="<?= Config::get('site.url') ?>/account/messages.php?id=<?= $conv->getSecondUser()->getId(); ?>">
									<td><?= htmlspecialchars($conv->getSecondUser()->getFullName()) ?></td>
									<td><?= date('d/m/Y H:i', $conv->getLastDate()); ?></td>
								</tr>
								<?php } ?>
							  </tbody>
							</table>
							<?php
									}
								}
							?>
				        </div>
				        <div class="row">
							<div class="col-12 mt-2">
								<nav aria-label="Page navigation example">
								  	<ul class="pagination justify-content-center">
								  		<?php
								  		//Si la page est la première, on affiche pas le bouton précédent
								  		if($page != 1) {
								  		?>
								    	<li class="page-item">
								      		<a class="page-link" href="?page=<?= ($page - 1) ?>" aria-label="Previous">
								        		<span aria-hidden="true">&laquo;</span>
								        		<span class="sr-only">Previous</span>
								      		</a>
								    	</li>
								    	<?php } 

								    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
								    	if($page < 5) {
								    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
								    	?>
										<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
										<?php 
											}

											//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
											if($pageCount > 5) {
										?>
										<li class="page-item disabled"><a class="page-link">...</a></li>
										<li class="page-item"><a class="page-link" href="?page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
										<?php
											}
										} else if($page > $pageCount - 5) {
											//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
											if($pageCount - 5 > 1) {
										?>
										<li class="page-item"><a class="page-link" href="?page=1">1</a></li>
										<li class="page-item disabled"><a class="page-link">...</a></li>
										<?php 
											}
											for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
										?>
										<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
										<?php
											}
										}

										//Si on est pas sur la dernière page, on affiche le bouton suivant
										if($page != $pageCount) {
								    	?>
								    	<li class="page-item">
								      		<a class="page-link" href="?page=<?= ($page + 1) ?>" aria-label="Next">
								        		<span aria-hidden="true">&raquo;</span>
								        		<span class="sr-only">Next</span>
								      		</a>
								    	</li>
								    	<?php } ?>
								  	</ul>
								</nav>
							</div>
						</div>
				    </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php include("../templates/footer.php"); ?>