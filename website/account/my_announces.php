<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Page permettant à l'utilisateur de visualiser toutes ses annonces.
	*/
	
	require_once("../core.php");

	$pageName = "my_announces";

	$announces = array();
	$page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
	$announcesPerPage = 12;
	$announcesCount = 0;
	$pageCount = 1;

	if($user == null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	//Annulation d'une annonce
	if(isset($_GET['stop_id']) && $tokenValid) {
		$an = Announce::findById($_GET['stop_id']);
		//On vérifie que l'anonce est valide et qu'elle appartient à l'utilisateur
		if($an != null && $an->getSender()->getId() == $user->getId() && $an->getValid() && !$an->getDeleted()) {
			//Annulation de l'annonce
			$an->setDeleted(true);
			$an->save();
			array_push($notifications, array('type' => 'success', 'title' => 'ANNONCE', 'content' => 'Votre annonce a bien été annulée.', 'time' => 3000));
		}
	}

	$announces = Announce::getUserAnnounces($user, $page, $announcesPerPage);
	$announcesCount = Announce::getUserAnnouncesCount($user);
	$pageCount = ceil($announcesCount / $announcesPerPage);

	include("../templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<div class="container mt-3 mb-3 h-100">
	<div class="card bg-light">
		<div class="card-body">
			<div class="container">
			    <div class="row">
			        <?php include('../templates/account_nav.php'); ?>
			        <div class="col-lg-8 pb-5">
			        	<div class="row col-lg-12">
			        		<span class="font-weight-bold">Annonces en ligne: <?= number_format(Announce::getUserAnnouncesValidCount($user->getId()), 0, '.', ' ') ?>, Total: <?= $announcesCount ?></span>
			        	</div>
			        	<hr class="mt-2">
			        	<div class="row">
							<div class="col-12 mt-2">
								<nav aria-label="Page navigation example">
								  	<ul class="pagination justify-content-center">
								  		<?php
								  		//Si la page est la première, on affiche pas le bouton précédent
								  		if($page != 1) {
								  		?>
								    	<li class="page-item">
								      		<a class="page-link" href="?page=<?= ($page - 1) ?>" aria-label="Previous">
								        		<span aria-hidden="true">&laquo;</span>
								        		<span class="sr-only">Previous</span>
								      		</a>
								    	</li>
								    	<?php } 

								    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
								    	if($page < 5) {
								    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
								    	?>
										<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
										<?php 
											}

											//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
											if($pageCount > 5) {
										?>
										<li class="page-item disabled"><a class="page-link">...</a></li>
										<li class="page-item"><a class="page-link" href="?page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
										<?php
											}
										} else if($page > $pageCount - 5) {
											//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
											if($pageCount - 5 > 1) {
										?>
										<li class="page-item"><a class="page-link" href="?page=1">1</a></li>
										<li class="page-item disabled"><a class="page-link">...</a></li>
										<?php 
											}
											for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
										?>
										<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
										<?php
											}
										}

										//Si on est pas sur la dernière page, on affiche le bouton suivant
										if($page != $pageCount) {
								    	?>
								    	<li class="page-item">
								      		<a class="page-link" href="?page=<?= ($page + 1) ?>" aria-label="Next">
								        		<span aria-hidden="true">&raquo;</span>
								        		<span class="sr-only">Next</span>
								      		</a>
								    	</li>
								    	<?php } ?>
								  	</ul>
								</nav>
							</div>
						</div>
			        	<div class="row">
							<?php 
							foreach($announces as $announce) {
							?> 
							<div class="col-12 col-md-6 mb-2">
								<a class="text-dark text-left announce-link" href="<?php echo Config::get('site.url'); ?>/announce.php?id=<?= $announce->getId() ?>" title="<?= htmlspecialchars($announce->getTitle()) ?>">
									<div class="card">
										<div class="embed-responsive embed-responsive-16by9">
										<div class="card-img-top embed-responsive-item text-center announce-grey-card">
									  		<img class="announce-card-img" src="<?php echo Config::get('site.url'); ?>/images/<?= $announce->getFirstImage() ?>">
									  	</div>
									  </div>
									  <div class="card-body">
									  		<div class="alert alert-<?= (!$announce->getValid() || $announce->getDeleted() ? "danger" : 
									  									($announce->getDate() > (time() - 5256000) ? "success" :
									  									"warning")) ?> font-weight-bold">
								  				<?= 	(!$announce->getValid() ? "Annonce non validée" :
								  						($announce->getDeleted() ? "Annonce annulée" :
								  						($announce->getDate() > (time() - 5256000) ? "Annonce en ligne:<br /><a href=\"?stop_id=".$announce->getId()."&token=".$_SESSION['token']."\">Annuler la vente</a>" :

								  						 "Annonce dépassée"))) ?>
											</div>
									    	<h6 class="card-title"><?= htmlspecialchars($announce->getTitle()) ?></h6>
									    	<p class="card-text announce-card-desc">
									    		<?php $announce->showCategoryName(); ?><br />
									    		<?php $announce->showCityName(); ?><br />
									    		<?php $announce->showDate(); ?>
									    	</p>
									    	<span class="badge badge-dark font-weight-bold float-left"><i class="fa fa-camera" style="margin-right: 5px;"></i> <?= $announce->getImgCount() ?></span>
									    	<?php 
									    	if($announce->getPrice() > 0) 
									    		echo '<span class="badge badge-dark font-weight-bold float-right">'.number_format($announce->getPrice(), 2, ',', ' ').' <i class="fa fa-euro-sign"></i></span>';
									    	else
									    		echo '<span class="badge badge-dark font-weight-bold float-right">GRATUIT</span>';
									    	?>						    	
									  </div>
									</div>
								</a>
							</div>
							<?php 
							} 
							?>
						</div>
						<div class="row">
							<div class="col-12 mt-2">
								<nav aria-label="Page navigation example">
								  	<ul class="pagination justify-content-center">
								  		<?php
								  		//Si la page est la première, on affiche pas le bouton précédent
								  		if($page != 1) {
								  		?>
								    	<li class="page-item">
								      		<a class="page-link" href="?page=<?= ($page - 1) ?>" aria-label="Previous">
								        		<span aria-hidden="true">&laquo;</span>
								        		<span class="sr-only">Previous</span>
								      		</a>
								    	</li>
								    	<?php } 

								    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
								    	if($page < 5) {
								    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
								    	?>
										<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
										<?php 
											}

											//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
											if($pageCount > 5) {
										?>
										<li class="page-item disabled"><a class="page-link">...</a></li>
										<li class="page-item"><a class="page-link" href="?page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
										<?php
											}
										} else if($page > $pageCount - 5) {
											//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
											if($pageCount - 5 > 1) {
										?>
										<li class="page-item"><a class="page-link" href="?page=1">1</a></li>
										<li class="page-item disabled"><a class="page-link">...</a></li>
										<?php 
											}
											for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
										?>
										<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
										<?php
											}
										}

										//Si on est pas sur la dernière page, on affiche le bouton suivant
										if($page != $pageCount) {
								    	?>
								    	<li class="page-item">
								      		<a class="page-link" href="?page=<?= ($page + 1) ?>" aria-label="Next">
								        		<span aria-hidden="true">&raquo;</span>
								        		<span class="sr-only">Next</span>
								      		</a>
								    	</li>
								    	<?php } ?>
								  	</ul>
								</nav>
							</div>
						</div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php include("../templates/footer.php"); ?>