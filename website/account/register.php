<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
	*/
	
	require_once("../core.php");

	$pageName = "register";

	if($user != null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	$result = array();

	if($tokenValid && isset($_POST['uFName']) && isset($_POST['uName']) && isset($_POST['uMail']) && isset($_POST['uRMail']) && isset($_POST['uPass']) && isset($_POST['uRPass'])) {
		$error = false;

		$forename = $_POST['uFName'];
		$name = $_POST['uName'];
		$mail = $_POST['uMail'];
		$rMail = $_POST['uRMail'];
		$pass = $_POST['uPass'];
		$rpass = $_POST['uRPass'];

		/* Verifications adresse mail */
		if(filter_var($_POST['uMail'], FILTER_VALIDATE_EMAIL)) {
			//Verification: l'adresse mail est-elle utilisée par un autre utilisateur ?
			if(User::findByMail($_POST['uMail']) != null) { //Adresse mail déjà utilisée !

				$result[] = array('type' => 'danger', 'content' => 'Adresse e-mail déjà utilisée.');
				$error = true;

			}

		} else  { 
			$result[] = array('type' => 'danger', 'content' => 'Adresse e-mail invalide.');
			$error = true;
		}

		if($mail != $rMail) {
			$result[] = array('type' => 'danger', 'content' => 'Les adresses mail ne correspondent pas.');
			$error = true;
		}

		/* Verifications prénom */
		if(strlen($forename) <= 2) {
			$result[] = array('type' => 'danger', 'content' => 'Prénom trop court.');
			$error = true;
		} 
		else if(strlen($forename) > 50) {
			$result[] = array('type' => 'danger', 'content' => 'Prénom trop long.');
			$error = true;
		}
		else if (!preg_match("/^[a-zA-Z ]*$/", $forename)) {
			$result[] = array('type' => 'danger', 'content' => 'Le prénom n\'est pas valide.');
			$error = true;
		}

		/* Verifications nom */
		if(strlen($name) <= 2) {
			$result[] = array('type' => 'danger', 'content' => 'Nom trop court.');
			$error = true;
		} 
		else if(strlen($name) > 50) {
			$result[] = array('type' => 'danger', 'content' => 'Nom trop long.');
			$error = true;
		}
		else if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
			$result[] = array('type' => 'danger', 'content' => 'Le nom n\'est pas valide.');
			$error = true;
		}

		/* Verifications mot de passe */
		if(strlen($pass) < 5) {
			$result[] = array('type' => 'danger', 'content' => 'Mot de passe trop court.');
			$error = true;
		} 
		else if($pass != $rpass) {
			$result[] = array('type' => 'danger', 'content' => 'Les mots de passe ne correspondent pas.');
			$error = true;
		}

		/* Pas d'erreur création du compte */
		if(!$error) {

			$mailCode = bin2hex(random_bytes(50));
			$newUser = new User();
			$newUser->setMail($mail);
			$newUser->setPass(hash("sha256", $pass));
			$newUser->setName($name);
			$newUser->setForename($forename);
			$newUser->setActiv(Config::get('mail.activation') == false);
			$newUser->setActivCode($mailCode);
			$newUser->setRegisterIp($_SERVER['REMOTE_ADDR']);
			$newUser->setLastIp($_SERVER['REMOTE_ADDR']);
			$newUser->save();

			if(Config::get('mail.activation')) {
				$activMail = new Mail();
				$activMail->setReceiver($newUser->getMail());
				$activMail->setSubject("Activation du compte");
				$activMail->setData([
						'%name%' => $newUser->getName(),
						'%forename%' => $newUser->getForename(),
						'%link%' => Config::get('site.url').'/account/?activate='.$newUser->getActivCode(),
						'%sitename%' => Config::get('site.name')
					]);
				$activMail->setModel("register.mail");
				$activMail->setAltBody("Bienvenue sur ".Config::get('site.name').",\n lien d'activation : ".Config::get('site.url')."/account/?activate=".$newUser->getActivCode());

				$activMail->send();

				$result[] = array('type' => 'success', 'content' => 'Inscription terminée, un mail de confirmation a été envoyer à '.htmlspecialchars($mail).'.');
			}
			else
			{
				//Connexion au compte
				$_SESSION['mail'] = $mail;
				$_SESSION['pass'] = hash("sha256", $pass);

				redirect(Config::get('site.url')."/account");
				exit();
			}
		}
	}

	include("../templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<div class="container mt-3 mb-3 h-100">
	<div class="card bg-light">
		<div class="card-body">
			<div class="container">
			    <div class="row">
			        <div class="col-lg-12 pb-5">
			        	<div class="row">
			        		<?php 
			        		foreach ($result as $message) { 
			        		?>
						  	<div class="alert alert-<?php echo $message['type']; ?> alert-dismissible fase show col-12">
						  		<button type="button" class="close" data-dismiss="alert">&times;</button>
			  					<?php echo $message['content']; ?>
							</div>
						  	<?php } ?>
			        	</div>
			            <form class="row" method="post" action="<?php echo Config::get('site.url'); ?>/account/register.php">
			            	<div class="col-12">
			                    <h2>Inscription</h2>
			                    <hr class="mt-2 mb-3">
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-fn">Prénom</label>
			                        <input class="form-control" type="text" id="account-fn" name="uFName" value="<?php echo isset($_POST['uFName']) ? htmlspecialchars($_POST['uFName']) : ""; ?>" required="">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-ln">Nom</label>
			                        <input class="form-control" type="text" id="account-ln" name="uName" value="<?php echo isset($_POST['uName']) ? htmlspecialchars($_POST['uName']) : ""; ?>" required="">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-email">Adresse e-mail</label>
			                        <input class="form-control" type="email" id="account-email" name="uMail" value="<?php echo isset($_POST['uMail']) ? htmlspecialchars($_POST['uMail']) : ""; ?>" required="">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-email-confirm">Confirmation adresse e-mail</label>
			                        <input class="form-control" type="email" id="account-email-confirm" name="uRMail" value="<?php echo isset($_POST['uRMail']) ? htmlspecialchars($_POST['uRMail']) : ""; ?>" required="">
			                    </div>
			                </div>
			                 <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-pass">Mot de passe</label>
			                        <input class="form-control" type="password" name="uPass" id="account-pass" value="" required="">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-pass-confirm">Confirmation mot de passe</label>
			                        <input class="form-control" type="password" name="uRPass" id="account-pass-confirm" value="" required="">
			                    </div>
			                </div>
			                <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
			                <div class="col-12">
			                    <hr class="mt-2 mb-3">
			                    <div class="d-flex flex-wrap justify-content-between align-items-center float-right">
			                        <button class="btn btn-style-1 btn-primary" type="submit">Inscription</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php include("../templates/footer.php"); ?>