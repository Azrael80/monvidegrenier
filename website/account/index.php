<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
	*/
	
	require_once("../core.php");

	$pageName = "account";


	if($user == null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	$result = array();

	if($tokenValid && isset($_POST['uMail']) && isset($_POST['uPass']) && isset($_POST['uNPass']) && isset($_POST['uReNPass']))
	{
		if($user->getPass() == hash("sha256", $_POST['uPass'])) {

			//Changement adresse e-mail
			if($_POST['uMail'] != $user->getMail())
			{
				if(filter_var($_POST['uMail'], FILTER_VALIDATE_EMAIL)) {
					//Verification: l'adresse mail est-elle utilisée par un autre utilisateur ?
					$request = Config::db()->prepare('SELECT uId FROM user WHERE uMail = :mail;');						
					$request->execute(array(':mail' => $_POST['uMail']));

					if($request->rowCount() <= 0) {

						$_SESSION['mail'] = $_POST['uMail'];
						$user->setMail($_SESSION['mail']);
						$result[] = array('type' => 'success', 'content' => 'Adresse e-mail mise à jour.');

					}else $result[] = array('type' => 'danger', 'content' => 'Adresse e-mail déjà utilisée !');

				} else $result[] = array('type' => 'danger', 'content' => 'Adresse e-mail invalide !');
			}

			//Changement mot de passe
			if(!empty($_POST['uNPass'])) {
				if($_POST['uReNPass'] == $_POST['uNPass']) {
					if(strlen($_POST['uNPass']) > 5) {

						$_SESSION['pass'] = hash("sha256", $_POST['uNPass']);
						$user->setPass($_SESSION['pass']);

						$result[] = array('type' => 'success', 'content' => 'Le mot de passe a été changer avec succès.');

					}
					else $result[] = array('type' => 'danger', 'content' => 'Le mot de passe doit faire au moins 6 caractères !');
				}
				else $result[] = array('type' => 'danger', 'content' => 'Les mots de passe ne correspondent pas !');
			}

			//Sauvegarde des modifications
			$user->save();
		}
		else $result[] = array('type' => 'danger', 'content' => 'Le mot de passe est incorrect !');
	}

	include("../templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<div class="container mt-3 mb-3 h-100">
	<div class="card bg-light">
		<div class="card-body">
			<div class="container">
			    <div class="row">
			        <?php include('../templates/account_nav.php'); ?>
			        <div class="col-lg-8 pb-5">
			        	<div class="row">
			        		<?php 
			        		foreach ($result as $message) { 
			        		?>
						  	<div class="alert alert-<?php echo $message['type']; ?> alert-dismissible fase show col-12">
						  		<button type="button" class="close" data-dismiss="alert">&times;</button>
			  					<?php echo $message['content']; ?>
							</div>
						  	<?php } ?>
			        	</div>
			            <form class="row" method="post" action="<?php echo Config::get('site.url'); ?>/account/">
			                <div class="col-md-6">
			                    <div class="form-group">
			                        <label for="account-fn">Prénom</label>
			                        <input class="form-control" type="text" id="account-fn" name="uFName" value="<?php echo htmlspecialchars($user->getForename()); ?>" disabled="">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-ln">Nom</label>
			                        <input class="form-control" type="text" id="account-ln" name="uName" value="<?php echo htmlspecialchars($user->getName()); ?>" disabled="">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-email">Adresse e-mail</label>
			                        <input class="form-control" type="email" id="account-email" name="uMail" value="<?php echo htmlspecialchars($user->getMail()); ?>" required="">
			                    </div>
			                </div>
			                 <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-actual-pass">Mot de passe actuel</label>
			                        <input class="form-control" type="password" name="uPass" id="account-actual-pass">
			                    </div>
			                </div>
			                <div class="col-12 col-md-6">
			                    <div class="form-group">
			                        <label for="account-pass">Nouveau mot de passe</label>
			                        <input class="form-control" type="password" name="uNPass" id="account-pass">
			                    </div>
			                </div>
			                <div class="col-12  col-md-6">
			                    <div class="form-group">
			                        <label for="account-confirm-pass">Confirmation du mot de passe</label>
			                        <input class="form-control" type="password" name="uReNPass" id="account-confirm-pass">
			                    </div>
			                </div>
			                <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
			                <div class="col-12">
			                    <hr class="mt-2 mb-3">
			                    <div class="d-flex flex-wrap justify-content-between align-items-center">
			                        <button class="btn custom btn-style-1 btn-primary" type="submit">Sauvegarder</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php include("../templates/footer.php"); ?>