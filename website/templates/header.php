<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Contien le haut de chaque page
	*/
?>
<!DOCTYPE HTML>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo Config::get('site.name'); ?></title>
		<link rel="stylesheet" href="<?= Config::get('site.url') ?>/files/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= Config::get('site.url') ?>/files/css/style.css">
		<script src="<?= Config::get('site.url') ?>/files/js/jquery-3.4.1.min.js"></script>
		<script src="<?= Config::get('site.url') ?>/files/js/popper.min.js"></script>
		<script src="<?= Config::get('site.url') ?>/files/js/bootstrap.min.js"></script>
		<script src="<?= Config::get('site.url') ?>/files/js/typeahead/typeahead.bundle.js"></script>
		<script src="<?= Config::get('site.url') ?>/files/js/scripts.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
		<!-- Leaflet -->
		<link rel="stylesheet" href="<?= Config::get('site.url') ?>/files/leaflet/leaflet.css" />
		<script src="<?= Config::get('site.url') ?>/files/leaflet/leaflet.js"></script>
		<script>
			var data = {};<?php 
			//Affichage des paramètres pour les scripts JS
			foreach ($scriptData as $key => $value) {
				if(!is_array($value)) {
					echo "\n\t\t\tdata.$key = '$value';";
				} else {
					//la valeur est un tableau => tableau/obj JS
					echo "\n\t\t\tdata.$key = {";
					foreach ($value as $key2 => $value2) {
						echo "\n\t\t\t\t$key2: '$value2',";
					}
					echo "\n\t\t\t};";
				}
			}
			echo "\n";
			?>
		</script>
	</head>
	<body>
		<nav class="navbar navbar-expand-sm navbar-custom sticky-top">
			<div class="container">
			<a class="navbar-brand" href="<?php echo Config::get('site.url'); ?>"><?php echo Config::get('site.name'); ?></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon">
					<span class="line" style="margin-top: 5px;"></span> 
					<span class="line"></span> 
					<span class="line"></span>
					<span class="line"></span>
				</span>
			</button>
			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav w-100">
					<li class="nav-item <?php if($pageName == "search") echo "active"; ?>">
						<a class="nav-link" href="<?php echo Config::get('site.url'); ?>"><i class="fa fa-search fa-lg" style="font-size: 0.8em;"></i> Rechercher</a>
					</li>
					<?php if($user != null) { ?>
					<li class="nav-item <?php if($pageName == "add_announce") echo "active"; ?>">
						<a class="nav-link" href="<?php echo Config::get('site.url'); ?>/add_announce.php"><i class="fa fa-plus-square fa-lg" style="font-size: 0.8em;"></i> Déposer une annonce</a>
					</li>
					<li class="nav-item dropdown ml-auto d-none d-sm-block <?php if($pageName == 'account' || $pageName == 'my_announces' || $pageName == 'messages') echo "active"; ?>">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg" style="font-size: 0.8em;"></i> <?= htmlspecialchars($user->getFullName()); ?> </a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="<?php echo Config::get('site.url'); ?>/account/"><i class="fa fa-user-cog fa-lg" style="font-size: 0.8em;"></i> Mon compte</a>
							<a class="dropdown-item" href="<?php echo Config::get('site.url'); ?>/account/my_announces.php"><i class="fa fa-sticky-note fa-lg" style="font-size: 0.8em;"></i> Mes annonces</a>
							<a class="dropdown-item" href="<?php echo Config::get('site.url'); ?>/account/messages.php"><i class="fa fa-envelope fa-lg" style="font-size: 0.8em;"></i> Messagerie <span class="badge badge-dark"><?= $user->getUnReadPMCount(); ?></span></a>
							<a class="dropdown-item" href="?action=logout">Déconnexion <i class="fa fa-sign-out-alt fa-lg" style="font-size: 0.8em;"></i></a>
						</div>
					</li>
					<li class="nav-item ml-auto d-block d-sm-none <?php if($pageName == 'account' || $pageName == 'messages') echo "active"; ?>">
						<a class="nav-link" href="<?php echo Config::get('site.url'); ?>/account/"><i class="fa fa-user fa-lg" style="font-size: 0.8em;"></i> Mon compte <span class="badge badge-dark"><?= $user->getUnReadPMCount(); ?></span></a>
					</li>
					<?php } else { ?>
					<li class="nav-item">
						<a class="nav-link" href="#" data-toggle="modal" data-target="#connectionModal"><i class="fa fa-plus-square fa-lg" style="font-size: 0.8em;"></i> Déposer une annonce</a>
					</li>
					<li class="nav-item ml-auto">
						<a class="nav-link" href="#" data-toggle="modal" data-target="#connectionModal">Connexion <i class="fa fa-user fa-lg" style="font-size: 0.8em;"></i> </a>
					</li>
					<?php } ?>
				</ul>
			</div>
			</div>
		</nav>
		<div class="alert alert-warning text-center" role="alert" id="javascriptWarning">
		  	Vous devez activer javascript avec une version récente pour profiter pleinement de <?= Config::get('site.name') ?>.
		</div>
		<script>$('#javascriptWarning').alert('close');</script>