<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
	*/
?>
<div class="modal fade" id="connectionModal">
		  <div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
			  <div class="modal-header">
				<h4 class="modal-title" id="connexionModalTitle"><?php echo (isset($_POST['forgot']) ? "Mot de passe oublié ?" : "Connexion"); ?></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="showAndClose('#connexionBlock', '#forgotBlock', '#connexionModalTitle', 'Connexion');">&times;</button>
			  </div>
			  <div id="connexionBlock" class="modal-body" <?php if(isset($_POST['forgot'])) echo 'style="display: none;"'; ?>>
			  	<?php if(isset($connection_error)) { ?>
			  	<div class="alert alert-danger alert-dismissible fase show">
			  		<button type="button" class="close" data-dismiss="alert">&times;</button>
  					Adresse mail ou mot de passe incorrect !
				</div>
			  	<?php } ?>
				<form method="post" action="#">
					<div class="form-group">
						<label for="mail">Adresse mail:</label>
						<input type="email" class="form-control" placeholder="Entrer adresse mail" name="mail" id="mail" value="<?php if(isset($_POST['mail'])) echo htmlspecialchars($_POST['mail']); ?>">
					</div>
					<div class="form-group">
						<label for="pass">Mot de passe:</label>
						<input type="password" class="form-control" placeholder="Entrer mot de passe" name="pass" id="pass">
					</div>
					<div class="form-group">
						<a href="#" onclick="showAndClose('#forgotBlock','#connexionBlock', '#connexionModalTitle', 'Mot de passe oublié ?');">Mot de passe oublié ?</a>
					</div>
					<a class="btn btn-info" role="button" href="<?php echo Config::get('site.url'); ?>/account/register.php">Inscription</a>
					<input type="submit" class="btn btn-success float-right" name="connexion" value="Connexion"/>
				</form>
			  </div>
			  <div id="forgotBlock" class="modal-body" <?php if(!isset($_POST['forgot'])) echo 'style="display: none;"'; ?>>
			  	<?php if(isset($forgot_ok)) { ?>
			  	<div class="alert alert-success alert-dismissible fase show">
			  		<button type="button" class="close" data-dismiss="alert">&times;</button>
  					Un mail vous a été envoyer afin de récupérer votre mot de passe.
				</div>
			  	<?php } ?>
			  	<form method="post" action="#">
					<div class="form-group">
						<label for="mail">Adresse mail:</label>
						<input type="email" class="form-control" placeholder="Entrer adresse mail" name="mail" id="mail" value="<?php if(isset($_POST['mail'])) echo htmlspecialchars($_POST['mail']); ?>">
					</div>
					<a class="btn btn-info" role="button" href="#" onclick="showAndClose('#connexionBlock', '#forgotBlock', '#connexionModalTitle', 'Connexion');">&laquo; Retour</a>
					<input type="submit" class="btn btn-success float-right" name="forgot" value="Envoyer"/>
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="showAndClose('#connexionBlock', '#forgotBlock', '#connexionModalTitle', 'Connexion');">Fermer</button>
			  </div>
			</div>
		  </div>
		</div>
		<footer class="page-footer font-small blue">
			<div class="footer-copyright text-center py-3">© 2020 Copyright:
			    CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim - L2INFO Prog. web
			</div>
		</footer>
		<?php foreach($notifications as $notif) { ?>
		<div class="toast toast-color" id="notification" data-delay="<?php echo $notif['time']; ?>">
			<div class="toast-header toast-color"> 
	            <strong class="mr-auto"><?php echo $notif['title']; ?></strong> 
	              
	            <button type="button" class="ml-2 mb-1 close"
	                    data-dismiss="toast" aria-label="Close"> 
	                <span aria-hidden="true">×</span> 
	            </button> 
        	</div> 
	        <div class="toast-body"> 
	           	<?php echo $notif['content']; ?>
	        </div> 
    	</div> 
  		<?php } ?>
	    <script> 
	        $(document).ready(function() { 
	        	<?php if(isset($connection_error) || isset($_POST['forgot'])) { ?>$('#connectionModal').modal('show');<?php } ?>
	            $('.toast').toast('show');  
	        }); 
	    </script> 
	</body>
</html>