<div class="col-lg-4 pb-5">
    <div class="wizard">
        <nav class="list-group list-group-flush">
            <a class="list-group-item <?php if($pageName == 'account') echo 'active custom'; ?>" href="<?php echo Config::get('site.url'); ?>/account/">
                <i class="fa fa-user-cog fa-lg"></i> Paramètres
            </a>
            <a class="list-group-item <?php if($pageName == 'my_announces') echo 'active custom'; ?>" href="<?php echo Config::get('site.url'); ?>/account/my_announces.php">
                <i class="fa fa-sticky-note fa-lg"></i> Mes annonces <span class="badge badge-dark float-right"><?= Announce::getUserAnnouncesValidCount($user->getId()) ?></span>
            </a>
            <a class="list-group-item <?php if($pageName == 'messages') echo 'active custom'; ?>" href="<?php echo Config::get('site.url'); ?>/account/messages.php">
                <i class="fa fa-envelope fa-lg"></i> Messagerie <span class="badge badge-dark float-right"><?= $user->getUnReadPMCount(); ?></span>
            </a>
            <a class="list-group-item" href="?action=logout">
                <i class="fa fa-sign-out-alt fa-lg"></i> Déconnexion
            </a>
        </nav>
    </div>
</div>