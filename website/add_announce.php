<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Page pour l'ajout d'une annonce
	*/
	
	require_once("./core.php");

	$pageName = "add_announce";

	if($user == null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	$result = array();
	$errors = array();

	$title = isset($_POST['title']) ? $_POST['title'] : "";
	$catId = isset($_POST['category']) ? $_POST['category'] : -1;
	$cityId = isset($_POST['cityId']) ? $_POST['cityId'] : 0;
	$cityName = "";
	$phone = isset($_POST['number']) ? $_POST['number'] : "";
	$price = isset($_POST['price']) ? $_POST['price'] : "";
	$desc = isset($_POST['desc']) ? $_POST['desc'] : "";

	if($tokenValid && isset($_POST['addAnnounce'])) {

		//Vérification de la taille du titre
		if(strlen($title) > 100 || strlen($title) < 5) {
			$errors['title'] = "Le titre doit faire entre 5 et 100 caractères.";
		}

		$request = Config::db()->prepare('SELECT * FROM category WHERE catId = ?');
		$request->execute([$catId]);
		$cat = $request->fetch(PDO::FETCH_OBJ);

		//Verification de la catégorie (il doit s'agir d'une sous-catégorie donc on vérifie aussi l'id parent)
		if($catId == -1 || $cat == null || $cat->catParentId < 1) {
			$errors['category'] = "Veuillez selectionner une catégorie.";
		}

		$request = Config::db()->prepare('SELECT * FROM ville WHERE ville_id = ?');
		$request->execute([$cityId]);
		$city = $request->fetch(PDO::FETCH_OBJ);

		//Vérification ville (on vérifie que la ville existe)
		if($cityId == 0 || $city == null) {
			$errors['city'] = "Veuillez selectionner une ville.";
		} else {
			$cityName = $city->ville_nom_reel.", ".explode('-', $city->ville_code_postal)[0];
		}

		//Vérification numéro de téléphone français : 0 + 9 chiffres
		if(!preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#", $phone)) {
		  	$errors['phone'] = "Veuillez entrer un numéro valide.";
		} else {
			//On vire les tirets, points et espaces
			$phone = str_replace(["-", ".", " "], "", $phone);
		}

		//Vérification du prix, prix non négatif, 2 chiffre après la virgule etc..
		$price = str_replace([',', ' '], ['.', ''], $price);
		if(!is_numeric($price) || $price != number_format($price, 2, '.', '') || $price < 0) {
			$errors['price'] = "Veuillez entrer un prix valide.";
		}

		//Vérifications sur les images
		if(isset($_FILES['img1']) && $_FILES['img1']['error'] != 4 && !checkImageExtension($_FILES['img1'])) {
			$errors['img1'] = "L'image n'est pas valide.";
		}

		if(isset($_FILES['img2']) && $_FILES['img2']['error'] != 4 && !checkImageExtension($_FILES['img2'])) {
			$errors['img2'] = "L'image n'est pas valide.";
		}

		if(isset($_FILES['img3']) && $_FILES['img3']['error'] != 4 && !checkImageExtension($_FILES['img3'])) {
			$errors['img3'] = "L'image n'est pas valide.";
		}

		//Vérification longueur description entre 50 et 5000 caractères
		if(strlen($desc) > 5000 || strlen($desc) < 50) {
			$errors['desc'] = "La description doit faire entre 50 et 5000 caractères.";
		}

		//Pas d'erreur on envoie l'annonce
		if(count($errors) == 0) {
			$announce = new Announce();
			$announce->setUId($user->getId());
			$announce->setCatId($cat->catId);
			$announce->setVilleId($city->ville_id);
			$announce->setTitle($title);
			$announce->setDesc($desc);
			$announce->setPhone($phone);
			$announce->setPrice($price);
			$announce->setValid(Config::get('announce.need.confirm') == false);

			//Envoie des images
			$images = array();

			if(isset($_FILES['img1']) && $_FILES['img1']['error'] != 4) {
				$images[] = uploadImage($_FILES['img1']); //Ajout de l'identifiant de l'image 1 dans le tableau
			}

			if(isset($_FILES['img2']) && $_FILES['img2']['error'] != 4) {
				$images[] = uploadImage($_FILES['img2']); //Ajout de l'identifiant de l'image 2 dans le tableau
			}

			if(isset($_FILES['img3']) && $_FILES['img3']['error'] != 4) {
				$images[] = uploadImage($_FILES['img3']); //Ajout de l'identifiant de l'image 3 dans le tableau
			}

			$announce->setValid(Config::get('announce.need.confirm') == false);
			$announce->setImages($images);
			$announce->save();

			//Redirection vers l'annonce
			redirect(Config::get('site.url')."/announce.php?id=".$announce->getId());
			exit();
		}
	}

	include("./templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<script>
$(document).ready(function($) {

	var cities = new Bloodhound({
	  	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: data['url'] + "/ajax/announces.ajax.php?action=selectCity&name=-1",
		remote: {
		    url: data['url'] + "/ajax/announces.ajax.php?action=selectCity&name=%QUERY",
		    wildcard: '%QUERY'
		}
	});

	$('#city-typeahead .typeahead').typeahead(null, {
	  	name: 'cities',
	  	display: 'name',
	  	source: cities
	});



	$('#city-typeahead').on('typeahead:selected', function (e, item) {
	    $('#cityId').val(item.ville_id);
	});

});
</script>
<div class="container mt-3 mb-3 h-100">
	<div class="card bg-light">
		<div class="card-body">
			<div class="container">
			    <div class="row">
			        <div class="col-12 pb-5">
			        	<div class="row">
			        		<?php 
			        		foreach ($result as $message) { 
			        		?>
						  	<div class="alert alert-<?php echo $message['type']; ?> alert-dismissible fase show col-12">
						  		<button type="button" class="close" data-dismiss="alert">&times;</button>
			  					<?php echo $message['content']; ?>
							</div>
						  	<?php } ?>
			        	</div>
			            <form class="container-fluid" method="post" action="#" enctype="multipart/form-data">
			            	<div class="form-row">
				                <div class="col-12 form-group">
	    							<label for="title" class="font-weight-bold">Titre de l'anonce</label>
								    <input type="text" class="form-control<?= (isset($errors['title']) ? " is-invalid" : "") ?>" id="title" name="title" placeholder="Entrer le titre de l'annonce" value="<?= htmlspecialchars($title) ?>">
								    <?php if(isset($errors['title'])) { ?>
								    <div class="invalid-feedback">
							          	<?= $errors['title'] ?>
							        </div>
								    <?php } ?>
	  							</div>
	  						</div>
  							<div class="form-row">
	  							<div class="col-6 form-group">
								    <label for="categorySelector" class="font-weight-bold">Catégorie</label>
								    <select class="form-control<?= (isset($errors['category']) ? " is-invalid" : "") ?>" id="categorySelector">
								      <script>
								      	loadCategories(true, -1);
								      </script>
								    </select>
								</div>
								<div class="col-6 form-group" id="subCategories">
									<label for="subCategorySelector" class="font-weight-bold">Sous-catégorie</label>
								    <select class="form-control<?= (isset($errors['category']) ? " is-invalid" : "") ?>" id="subCategorySelector" name="category">
								    </select>
								    <?php if(isset($errors['category'])) { ?>
								    <div class="invalid-feedback">
							          	<?= $errors['category'] ?>
							        </div>
								    <?php } ?>
								</div>
							</div>
							<div class="form-row">
								<div class="col-4">
									<div class="form-group input-group">
										<div class="input-group-prepend">
								          	<div class="input-group-text"><i class="fa fa-mobile-alt"></i></div>
								        </div>
								        <input type="text" class="form-control<?= (isset($errors['phone']) ? " is-invalid" : "") ?>" id="number" name="number" placeholder="Numéro de téléphone" value="<?= htmlspecialchars($phone) ?>">
										<?php if(isset($errors['phone'])) { ?>
									    <div class="invalid-feedback">
								          	<?= $errors['phone'] ?>
								        </div>
									    <?php } ?>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group input-group" id="city-typeahead">
								        <input type="text" class="typeahead form-control<?= (isset($errors['city']) ? " is-invalid" : "") ?>" id="city" placeholder="<?= (isset($errors['city']) ? $errors['city'] : "Ville") ?>" value="<?= htmlspecialchars($cityName) ?>">
									</div>
								</div>
								<div class="col-4">
									<div class="form-group input-group">
								        <input type="text" class="form-control<?= (isset($errors['price']) ? " is-invalid" : "") ?>" id="price" name="price" placeholder="Prix" value="<?= htmlspecialchars($price) ?>">
								        <div class="input-group-append">
								          	<div class="input-group-text font-weight-bold">€</div>
								        </div>
								        <?php if(isset($errors['price'])) { ?>
									    <div class="invalid-feedback">
								          	<?= $errors['price'] ?>
								        </div>
									    <?php } ?>
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-4 form-group">
								    <label for="img1" class="font-weight-bold">Photo #1</label>
								    <input type="file" class="form-control-file<?= (isset($errors['img1']) ? " is-invalid" : "") ?>" id="img1" name="img1" accept=".jpg,.jpeg,image/x-png,image/gif,image/bmp">
								    <?php if(isset($errors['img1'])) { ?>
									    <div class="invalid-feedback">
								          	<?= $errors['img1'] ?>
								        </div>
									<?php } ?>
								</div>
								<div class="col-4 form-group">
								    <label for="img2" class="font-weight-bold">Photo #2</label>
								    <input type="file" class="form-control-file<?= (isset($errors['img2']) ? " is-invalid" : "") ?>" id="img2" name="img2" accept=".jpg,.jpeg,image/x-png,image/gif,image/bmp">
								    <?php if(isset($errors['img3'])) { ?>
									    <div class="invalid-feedback">
								          	<?= $errors['img3'] ?>
								        </div>
									<?php } ?>
								</div>
								<div class="col-4 form-group">
								    <label for="img3" class="font-weight-bold">Photo #3</label>
								    <input type="file" class="form-control-file<?= (isset($errors['img3']) ? " is-invalid" : "") ?>" id="img3" name="img3" accept=".jpg,.jpeg,image/x-png,image/gif,image/bmp">
								    <?php if(isset($errors['img3'])) { ?>
									    <div class="invalid-feedback">
								          	<?= $errors['img3'] ?>
								        </div>
									<?php } ?>
								</div>
							</div>
							<div class="form-group">
							    <label for="desc" class="font-weight-bold">Description</label>
							    <textarea class="form-control<?= (isset($errors['desc']) ? " is-invalid" : "") ?>" id="desc" rows="20" name="desc"><?= htmlspecialchars($desc) ?></textarea>
							    <?php if(isset($errors['desc'])) { ?>
								<div class="invalid-feedback">
								    <?= $errors['desc'] ?>
								</div>
								<?php } ?>
							</div>
							<input type="hidden" class="form-control" name="cityId" id="cityId" value="<?= htmlspecialchars($cityId) ?>">
							<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
							<input type="hidden" name="addAnnounce" value="true">
			               	<div class="from-row text-center">
			                  	<button class="btn custom btn-style-1 btn-primary" type="submit">Envoyer</button>
			                </div>
			            </form>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php include("./templates/footer.php"); ?>