<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Page pour l'ajout d'une annonce
	*/
	
	require_once("./core.php");

	$pageName = "profile";

	$profile = null;
	$announces = array();
	$page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
	$announcesPerPage = 12;
	$announcesCount = 0;
	$pageCount = 1;

	if(isset($_GET['id'])) {
		$profile = User::findById($_GET['id']);
	}

	//Redirection vers l'accueil si l'utilisateur est introuvable
	if($profile == null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	$announces = Announce::getUserAnnouncesValid($profile->getId(), $page, 12);
	$announcesCount = Announce::getUserAnnouncesValidCount($profile->getId());
	$pageCount = ceil($announcesCount / $announcesPerPage);

	include("./templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<div class="container mt-3 mb-3 h-100">
	<div class="row">
		<div class="col-12 col-lg-8">
			<div class="row pl-3 pr-3">
				<div class="col-12 p-0">
					<div class="card">
						<div class="card-body font-weight-bold">
						   	<span class="align-middle">Annonces: <?= number_format($announcesCount, 0, '.', ' ') ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 mt-2">
					<nav aria-label="Page navigation example">
					  	<ul class="pagination justify-content-center">
					  		<?php
					  		//Si la page est la première, on affiche pas le bouton précédent
					  		if($page != 1) {
					  		?>
					    	<li class="page-item">
					      		<a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= ($page - 1) ?>" aria-label="Previous">
					        		<span aria-hidden="true">&laquo;</span>
					        		<span class="sr-only">Previous</span>
					      		</a>
					    	</li>
					    	<?php } 

					    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
					    	if($page < 5) {
					    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
					    	?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php 
								}

								//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
								if($pageCount > 5) {
							?>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<li class="page-item"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
							<?php
								}
							} else if($page > $pageCount - 5) {
								//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
								if($pageCount - 5 > 1) {
							?>
							<li class="page-item"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=1">1</a></li>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<?php 
								}
								for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
							?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php
								}
							}

							//Si on est pas sur la dernière page, on affiche le bouton suivant
							if($page != $pageCount) {
					    	?>
					    	<li class="page-item">
					      		<a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= ($page + 1) ?>" aria-label="Next">
					        		<span aria-hidden="true">&raquo;</span>
					        		<span class="sr-only">Next</span>
					      		</a>
					    	</li>
					    	<?php } ?>
					  	</ul>
					</nav>
				</div>
			</div>
			<div class="row">
			<?php 
				foreach($announces as $announce) {
			?> 
				<div class="col-12 col-md-6 mb-4">
					<a class="text-dark text-left announce-link" href="<?= Config::get('site.url'); ?>/announce.php?id=<?= $announce->getId() ?>" title="<?= htmlspecialchars($announce->getTitle()) ?>">
						<div class="card">
							<div class="embed-responsive embed-responsive-16by9">
							<div class="card-img-top embed-responsive-item text-center announce-grey-card">
						  		<img class="announce-card-img" src="<?= Config::get('site.url'); ?>/images/<?= $announce->getFirstImage() ?>">
						  	</div>
						  </div>
						  <div class="card-body">
						    	<h6 class="card-title"><?= htmlspecialchars($announce->getTitle()) ?></h6>
						    	<p class="card-text announce-card-desc">
						    		<?php $announce->showCategoryName(); ?><br />
						    		<?php $announce->showCityName(); ?><br />
						    		<?php $announce->showDate(); ?>
						    	</p>
						    	<span class="badge badge-dark font-weight-bold float-left"><i class="fa fa-camera" style="margin-right: 5px;"></i> <?= $announce->getImgCount() ?></span>
						    	<?php 
						    	if($announce->getPrice() > 0) 
						    		echo '<span class="badge badge-dark font-weight-bold float-right">'.number_format($announce->getPrice(), 2, ',', ' ').' <i class="fa fa-euro-sign"></i></span>';
						    	else
						    		echo '<span class="badge badge-dark font-weight-bold float-right">GRATUIT</span>';
						    	?>						    	
						  </div>
						</div>
					</a>
				</div>
			<?php 
				} 
			?>
			</div>
			<div class="row">
				<div class="col-12 mt-2">
					<nav aria-label="Page navigation example">
					  	<ul class="pagination justify-content-center">
					  		<?php
					  		//Si la page est la première, on affiche pas le bouton précédent
					  		if($page != 1) {
					  		?>
					    	<li class="page-item">
					      		<a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= ($page - 1) ?>" aria-label="Previous">
					        		<span aria-hidden="true">&laquo;</span>
					        		<span class="sr-only">Previous</span>
					      		</a>
					    	</li>
					    	<?php } 

					    	//Si on est dans les 5 premières pages on affiche les 5 premières pages, "..." et la dernière page
					    	if($page < 5) {
					    		for ($i = 1; $i <= ($pageCount > 5 ? 5 : $pageCount); $i++) {
					    	?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php 
								}

								//Si il y a plus de 5 pages, on affiche les "..." et la dernière page
								if($pageCount > 5) {
							?>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<li class="page-item"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= $pageCount ?>"><?= $pageCount ?></a></li>
							<?php
								}
							} else if($page > $pageCount - 5) {
								//Si on est dans les 5 dernières pages on affiche la première page, "..." et les 5 dernières pages
								if($pageCount - 5 > 1) {
							?>
							<li class="page-item"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=1">1</a></li>
							<li class="page-item disabled"><a class="page-link">...</a></li>
							<?php 
								}
								for ($i = ($pageCount - 5 > 1 ? $pageCount - 5 : 1); $i <= $pageCount; $i++) {
							?>
							<li class="page-item<?= ($i == $page ? ' active' : '') ?>"><a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= $i ?>"><?= $i ?></a></li>
							<?php
								}
							}

							//Si on est pas sur la dernière page, on affiche le bouton suivant
							if($page != $pageCount) {
					    	?>
					    	<li class="page-item">
					      		<a class="page-link" href="?id=<?= $profile->getId() ?>&page=<?= ($page + 1) ?>" aria-label="Next">
					        		<span aria-hidden="true">&raquo;</span>
					        		<span class="sr-only">Next</span>
					      		</a>
					    	</li>
					    	<?php } ?>
					  	</ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="col-12 mt-3 col-lg-4 mt-lg-0">
			<div class="card">
				<div class="card-body text-center font-weight-bold">
					<h5 class="font-weight-bold mb-0"><?= htmlspecialchars($profile->getFullName()) ?></h5>
					<?= number_format(Announce::getUserAnnouncesValidCount($profile->getId()), 0, '.', ' ') ?> annonces en ligne<br />
					Inscrit depuis le <?= date('d/m/Y', $profile->getRegisterDate()) ?><br />
					<?php 
						//si l'utilisateur est connecté et que ce n'est pas son annonce, on lui propose d'envoyer un message (lié à l'annonce)
						if($user != null && $user->getId() != $profile->getId()) {
							echo '<a href="'.Config::get('site.url').'/account/messages.php?id='.$profile->getId().'"><button type="button" class="btn btn-info font-weight-bold w-75 mt-1"><i class="fa fa-comment fa-lg"></i> Envoyer un message</button></a>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("./templates/footer.php"); ?>