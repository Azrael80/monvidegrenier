<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Script php qui rassemblera tout ce dont le site a besoin pour fonctionner
	*/

	define("DEBUG", true);

	if(DEBUG) {
		//Si le debug activé on modifie l'affichage des erreurs
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
	}
	
	session_start();
	
	require_once "class/Config.php";
	require_once "class/User.php";
	require_once "class/Mail.php";
	require_once "class/Conversation.php";
	require_once "class/Announce.php";

	//PHPMailer
	require_once "lib/PHPMailer/Exception.php";
	require_once "lib/PHPMailer/PHPMailer.php";
	require_once "lib/PHPMailer/SMTP.php";

	$notifications = array(); //Tableau contenant les notifications

	//Tableau contenant les variables javascript à mettre en début de chaque script php
	$scriptData = array(
						'url' => Config::get('site.url'),
						'accessMapToken' => Config::get('mapbox.token'),
						'accountId' => -1
					); 
	
	$user = null;

	//Activation d'un compte
	if(isset($_GET['activate'])) {
		$ac = User::findByActivCode($_GET['activate']);
		if($ac != null) {
			$ac->setActiv(true);
			$ac->save();

			$_SESSION['mail'] = $ac->getMail();
			$_SESSION['pass'] = $ac->getPass();

			array_push($notifications, array('type' => 'success', 'title' => 'VALIDATION', 'content' => 'Le compte est maintenant valide.', 'time' => 3000));
		}
	}

	//Mot de passe perdu, demande de code pour changement de mot de passe.
	if(isset($_POST['forgot']) && isset($_POST['mail'])) {
		$ac = User::findByMail($_POST['mail']);
		if($ac != null) {
			$code = bin2hex(random_bytes(50));
			$ac->setPassCode($code);
			$ac->save();

			$fMail = new Mail();
			$fMail->setReceiver($ac->getMail());
			$fMail->setSubject("Mot de passe oublié");
			$fMail->setData([
					'%name%' => $ac->getName(),
					'%forename%' => $ac->getForename(),
					'%link%' => Config::get('site.url').'/?forgotCode='.$ac->getPassCode(),
					'%sitename%' => Config::get('site.name')
			]);
			$fMail->setModel("send.forgot.mail");
			$fMail->setAltBody("Bonjour,\n veuillez suivre ce lien pour créer un nouveau mot de passe : ".Config::get('site.url')."/?forgotCode=".$ac->getPassCode());

			$fMail->send();

			array_push($notifications, array('type' => 'success', 'title' => 'MOT DE PASSE', 'content' => 'Vous avez reçu un mail pour changer votre mot de passe.', 'time' => 3000));
			$forgot_ok = true;
		}
	}

	//Mot de passe oublié, utilisation de l'url de changement de mot de passe => Envoie d'un nouveau mdp à l'adresse mail.
	if(isset($_GET['forgotCode'])) {
		$ac = User::findByForgot($_GET['forgotCode']);
		if($ac != null) {
			$newPass = bin2hex(openssl_random_pseudo_bytes(4));
			$ac->setPass(hash("sha256", $newPass));
			$ac->setPassCode('');
			$ac->save();

			$fMail = new Mail();
			$fMail->setReceiver($ac->getMail());
			$fMail->setSubject("Nouveau mot de passe");
			$fMail->setData([
					'%name%' => $ac->getName(),
					'%forename%' => $ac->getForename(),
					'%pass%' => $newPass,
					'%sitename%' => Config::get('site.name')
			]);
			$fMail->setModel("new.pass.mail");
			$fMail->setAltBody("Bonjour,\n voilà votre nouveau mot de passe : ".$newPass);

			$fMail->send();

			array_push($notifications, array('type' => 'success', 'title' => 'MOT DE PASSE', 'content' => 'Un mail avec votre nouveau mot de pas a été envoyer.', 'time' => 3000));
		}
	}

	//Génération du token
	if(!isset($_SESSION['token'])) {
		$_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(16));
	}

	//Vérification du token 
	$tokenValid = isset($_POST['token']) && $_POST['token'] == $_SESSION['token'] || isset($_GET['token']) && $_GET['token'] == $_SESSION['token'];
	
	if(isset($_SESSION['mail']) && isset($_SESSION['pass'])) { //Récupération de l'utilisateur

		$user = User::connect($_SESSION['mail'], $_SESSION['pass']);
		
		if($user == null)
			session_destroy();
		else
			$scriptData['accountId'] = $user->getId();
	}
	
	if(isset($_POST['connexion']) && $user == null) { //Connexion possible depuis n'importe quelle page
		if(isset($_POST['mail']) && isset($_POST['pass'])) {
			
			$user = User::connect($_POST['mail'], hash("sha256", $_POST['pass']));
			
			if($user != null) {
				
				$_SESSION['mail'] = $_POST['mail'];
				$_SESSION['pass'] = hash("sha256", $_POST['pass']);
				
				//Enregistrement de la dernière adresse ip
				$user->setLastIp($_SERVER['REMOTE_ADDR']);
				$user->save();

				$scriptData['accountId'] = $user->getId();

				array_push($notifications, array('type' => 'success', 'title' => 'CONNEXION', 'content' => 'Connexion réussie', 'time' => 3000)); //On enregistre la notification de connexion.			
			} else $connection_error = true;
			
		} else  $connection_error = true;
	}
	
	//Déconnexion
	if(isset($_GET['action']) && $_GET['action'] == "logout") {
		session_destroy();
		redirect(Config::get('site.url'));
		exit();
	}

	$pageName = "";
	
?>