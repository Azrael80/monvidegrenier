<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Page pour l'ajout d'une annonce
	*/
	
	require_once("./core.php");

	$pageName = "announce";

	$announce = null;

	if(isset($_GET['id'])) {
		$announce = Announce::findById($_GET['id']);

		//Si l'annonce n'est plus valide et que l'utilisateur n'est pas le propriétaire, on considère que l'annonce n'existe pas.

		if($announce != null && (!$announce->getValid() || $announce->getDeleted() || $announce->getDate() < (time() - 5256000)) && ($user == null || $user->getId() != $announce->getSender()->getId())) {
			$announce = null;
		}
	}

	//Redirection vers l'accueil si l'annonce est introuvable
	if($announce == null)
	{
		redirect(Config::get('site.url'));
		exit();
	}

	//On ajoute les variables pour les scripts 
	$scriptData['announce'] = [
		'id' => $announce->getId(),
		'cityname' => $announce->getCity()->ville_nom_reel,
		'lat' => $announce->getCity()->ville_latitude_deg,
		'long' => $announce->getCity()->ville_longitude_deg,
		'senderId' => $announce->getSender()->getId()
	];
	$scriptData['token'] = $_SESSION['token'];

	include("./templates/header.php"); //Affichage du header (contenu de la balise head, barre de navigation)	
?>
<div class="container mt-3 mb-3 h-100">
	<div class="row">
		<div class="col-12 col-lg-8">
			<div class="card">
				<div class="card-body">
					<?php 
					//Si il y a des images on les affiches
					if($announce->getImgCount() > 0) {
					?>
					<div id="carouselAnnounce" class="carousel slide announce-img" data-ride="carousel" data-interval="false">
					  	<ol class="carousel-indicators">
					  		<?php
					  		for($i = 0; $i < count($announce->getImages()); $i++)
					  			echo "<li data-target=\"#carouselAnnounce\" data-slide-to=\"$i\"".($i == 0 ? " class=\"active\"" : "")."></li>";
					  		?>
					  	</ol>
					  	<div class="carousel-inner">
					  		<?php 
					  		foreach ($announce->getImages() as $key => $img) {
					  			echo "<div class=\"carousel-item".($key == 0 ? " active" : "")."\">
					      				<img class=\"d-block m-auto\" src=\"".Config::get('site.url')."/images/$img\">
					    			</div>";
					  		}
					  		?>
					  	</div>
					  	<?php 
					  	//Si il y a plus d'une image, on affiche les boutons précédent/suivant.
					  	if(count($announce->getImages()) > 1) {
					  	?>
					  	<a class="carousel-control-prev" href="#carouselAnnounce" role="button" data-slide="prev">
					    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    	<span class="sr-only">Précédent</span>
					  	</a>
					  	<a class="carousel-control-next" href="#carouselAnnounce" role="button" data-slide="next">
					    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
					    	<span class="sr-only">Suivant</span>
					  	</a>
					  	<?php 
						} 
						?>
					</div>
					<?php 
					}
					?>
					<h5 class="font-weight-bold mt-1"><?= htmlspecialchars($announce->getTitle()); ?></h5>
					<?php 
					if($announce->getPrice() > 0) 
						echo '<h5><span class="badge badge-dark font-weight-bold">'.number_format($announce->getPrice(), 2, ',', ' ').' <i class="fa fa-euro-sign"></i></span></h5>';
						else
						echo '<h5><span class="badge badge-dark font-weight-bold">GRATUIT</span></h5>';
					?>
					<p class="mb-0"><?php $announce->showDate(); ?></p>
					<hr />
					<h5 class="font-weight-bold">Description</h5>
					<p>
						<?= nl2br(htmlspecialchars($announce->getDesc())); ?>
					</p>
					<hr />
					<h5 class="font-weight-bold">Localisation</h5>
					<div id="localisation-map">

					</div>
				</div>
			</div>
		</div>
		<div class="col-12 mt-3 col-lg-4 mt-lg-0">
			<div class="card">
				<div class="card-body text-center">
					<h5 class="font-weight-bold mb-0"><?= htmlspecialchars($announce->getSender()->getFullName()) ?></h5>
					<a href="<?= Config::get('site.url') ?>/profile.php?id=<?= $announce->getSender()->getId() ?>"><?= number_format(Announce::getUserAnnouncesValidCount($announce->getSender()->getId()), 0, '.', ' ') ?> annonces en ligne</a><br />
					<div class="btn btn-primary font-weight-bold w-75 mt-1" id="phone-block" style="display: none;"></div>
					<button type="button" class="btn btn-primary font-weight-bold w-75 mt-1" onclick="getAnnounceNumber()" id="numberButton"><i class="fas fa-mobile-alt fa-lg"></i> Voir le numéro</button>
					<?php 
						//si l'utilisateur est connecté et que ce n'est pas son annonce, on lui propose d'envoyer un message (lié à l'annonce)
						if($user != null && $user->getId() != $announce->getSender()->getId()) {
							echo '<button type="button" class="btn btn-info font-weight-bold w-75 mt-1" id="announce-message-button"><i class="fa fa-comment fa-lg"></i> Envoyer un message</button>';
							echo '<div class="input-group w-75 m-auto pt-1" id="announce-message-form" style="display: none;">
									<textarea class="form-control" aria-label="Message" id="send-message-content"></textarea>
									<div class="input-group-append">
										<button class="btn custom btn-primary" type="submit" id="announce-message-send">Envoyer</button>
									</div>
								</div>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("./templates/footer.php"); ?>