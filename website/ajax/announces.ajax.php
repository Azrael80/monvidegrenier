<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Script php (ajax) pour les annonces avec javascript.
	*/

	require_once("../core.php");

	$action = isset($_GET['action']) ? $_GET['action'] : "";
	$response = array();

	switch ($action) {
		//Donne la liste des catégories, si l'identifient est différent de -1 donne la liste des sous-catégorie
		case 'selectCategory':
			if(isset($_POST['catId'])) {
				$catId = $_POST['catId'];
				if($catId == -1) {
					$request = Config::db()->prepare('SELECT * FROM category WHERE catParentId = -1 ORDER BY catId');
					$request->execute();
					while($c = $request->fetch(PDO::FETCH_OBJ))
					{
						$response[] = $c;
					}
				} else {
					$request = Config::db()->prepare('SELECT * FROM category WHERE catParentId = ? ORDER BY catId');
					$request->execute([$catId]);
					while($c = $request->fetch(PDO::FETCH_OBJ))
					{
						$response[] = $c;
					}
				}
			}
			break;
		case 'selectCity':
			//Donne la liste des villes, recherche à partir du nom, si le nom vaut -1, on donne toutes les villes
			if(isset($_GET['name'])) {
				$name = $_GET['name'];
				if($name != -1) {
					$request = Config::db()->prepare("SELECT * FROM ville WHERE ville_nom_reel LIKE ? ORDER BY CHAR_LENGTH(ville_nom_reel) LIMIT 0,5");
					$request->execute(["%$name%"]);
					while($v = $request->fetch(PDO::FETCH_OBJ))
					{
						$response[] = ['ville_id' => $v->ville_id,'name' => $v->ville_nom_reel.", ".explode('-', $v->ville_code_postal)[0]];
					}
				} else {
					$request = Config::db()->prepare("SELECT * FROM ville ORDER BY CHAR_LENGTH(ville_nom_reel)");
					$request->execute();
					while($v = $request->fetch(PDO::FETCH_OBJ))
					{
						$response[] = ['ville_id' => $v->ville_id, 'name' => $v->ville_nom_reel.", ".explode('-', $v->ville_code_postal)[0]];
					}
				}
			}
			break;
		case 'selectLocalisation':
			/*
				Sélectionne toutes les régions/départements/villes qui correspondent au nom recherché
				Retourne le résultat au format JSON
				local_type:
					-1: Toute la france
					0: Région
					1: Département
					2: Ville
			*/
			if(isset($_GET['name'])) {
				$name = $_GET['name'];
				$response[] = ['local_id' => -1, 'local_name' => "Toute la France", 'local_type' => -1];
				if($name != -1) {
					$request = Config::db()->prepare("
						SELECT 0 AS local_type, code_region AS local_id, nom_region AS local_name, 0 AS code FROM region WHERE nom_region LIKE :name 
						UNION 
						SELECT 1 AS local_type, departement_id AS local_id, departement_nom AS local_name, departement_code AS code FROM departement WHERE departement_nom LIKE :name
						UNION
						SELECT 2 AS local_type, ville_id AS local_id, ville_nom_reel AS local_name, ville_code_postal AS code FROM ville WHERE ville_nom_reel LIKE :name ORDER BY local_type ASC, CHAR_LENGTH(local_name)
						");
					$request->execute([':name' => "%$name%"]);
					$lname = "";

					while($l = $request->fetch(PDO::FETCH_OBJ))
					{
						$lname = $l->local_name;

						//La localisation est une ville, on met donc le format de ville
						if($l->local_type == 2)
							$lname = $l->local_name.', '.explode('-', $l->code)[0];
						//La localisation est un département, on met donc le format département
						else if($l->local_type == 1)
							$lname = $l->local_name.'('.$l->code.')';

						$response[] = ['local_id' => $l->local_id, 'local_name' => $lname, 'local_type' => $l->local_type];
					}
				} else {
					$request = Config::db()->prepare("
						SELECT 0 AS local_type, code_region AS local_id, nom_region AS local_name, 0 AS code FROM region 
						UNION 
						SELECT 1 AS local_type, departement_id AS local_id, departement_nom AS local_name, departement_code AS code FROM departement
						UNION
						SELECT 2 AS local_type, ville_id AS local_id, ville_nom_reel AS local_name, ville_code_postal AS code FROM ville ORDER BY local_type ASC, CHAR_LENGTH(local_name)
						");
					$request->execute();
					$lname = "";

					while($l = $request->fetch(PDO::FETCH_OBJ))
					{
						$lname = $l->local_name;

						//La localisation est une ville, on met donc le format de ville
						if($l->local_type == 2)
							$lname = $l->local_name.', '.explode('-', $l->code)[0];
						//La localisation est un département, on met donc le format département
						else if($l->local_type == 1)
							$lname = $l->local_name.'('.$l->code.')';

						$response[] = ['local_id' => $l->local_id, 'local_name' => $lname, 'local_type' => $l->local_type];
					}
				}
			}
			break;
		case 'getPhone': //Affiche le numéro du propriétaire de l'annonce
			if(isset($_POST['id']) && $tokenValid) {
				$announce = Announce::findById($_POST['id']);
				if($announce != null) {
					$response['phone'] = substr(chunk_split($announce->getPhone(), 2, "."), 0, 14);
				}
			}
			break;
	}

	//Affichage réponse JSON
	header('Content-Type: application/json');
	echo json_encode($response);
?>