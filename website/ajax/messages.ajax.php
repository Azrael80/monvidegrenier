<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Script php (ajax) pour les messages avec javascript.
	*/

	require_once("../core.php");

	//Pas de script si l'utilisateur n'est pas connecté.
	if($user == null) exit;

	$action = isset($_GET['action']) ? $_GET['action'] : "";
	$response = array();

	switch ($action) {
		case 'send':
			//Envoie d'un message depuis une requête post envoyer avec javascript, retourne 2 si le message est envoyé, sinon erreur
			$receiver = isset($_POST['receiver']) ? User::findById($_POST['receiver']) : null;
			$message = isset($_POST['message']) && !empty($_POST['message']) ? $_POST['message'] : "";
			if($receiver != null && $receiver->getId() != $user->getId()) {
				$conversation = new Conversation($user, $receiver);
				if(strlen($message) > 0) {
					if(strlen($message) < 255) {
						//Enregistrement du message
						$conversation->addMessage($message);
						$response['result'] = 2;
					} else {
						$response['result'] = 1;
						$response['message'] = 'Le message est trop long!';
					}
				} else {
					$response['result'] = 3;
				}
			}
			else $response['result'] = 0; //Receveur non trouvé
			break;
			case 'send-announce':
			/*
				Envoie d'un message depuis une requête post envoyer avec javascript depuis l'affichage d'une annonce, retourne 2 si le message est envoyé, sinon erreur.
				Message avec annonce liée.
			*/
			$receiver = isset($_POST['receiver']) ? User::findById($_POST['receiver']) : null;
			$message = isset($_POST['message']) && !empty($_POST['message']) ? $_POST['message'] : "";
			$announceId = isset($_POST['announceId']) && !empty($_POST['announceId']) ? $_POST['announceId'] : "";
			$announce = Announce::findById($announceId);
			//On vérifie que l'annonce existe et que le receveur est bien celui qui a créer l'annonce
			if($receiver != null && $receiver->getId() != $user->getId() && $announce != null && $announce->getSender()->getId() == $receiver->getId()) {
				$conversation = new Conversation($user, $receiver);
				if(strlen($message) > 0) {
					if(strlen($message) < 255) {
						//Enregistrement du message
						$conversation->addMessage($message, $announce->getId());
						$response['result'] = 2;
					} else {
						$response['result'] = 1;
						$response['message'] = 'Le message est trop long!';
					}
				} else {
					$response['result'] = 3;
				}
			}
			else $response['result'] = 0; //Receveur non trouvé
			break;
		case 'get':
			//Récupère tous les messages à partir du dernier message affiché sur la page qui demande
			$receiver = isset($_POST['receiver']) ? User::findById($_POST['receiver']) : null;
			$lastId = isset($_POST['lastId']) ? $_POST['lastId'] : "";
			$response['lastId'] = $lastId;
			if($receiver != null && $receiver->getId() != $user->getId() && $lastId != "") {
				$conversation = new Conversation($user, $receiver);
				$response = $conversation->loadMessagesAfterIdAJAX($lastId);
				$conversation->view();
			}
		break;
	}

	//Affichage réponse JSON
	header('Content-Type: application/json');
	echo json_encode($response);
?>