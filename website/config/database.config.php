<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Configuration de la base de donnée
	*/
	

	return array(
					'db.host' => "localhost", 	//Hôte base de donnée
					'db.port' => "3306",		//Port d'accès base de donnée
					'db.user' => "root",		//Utilisateur base de donnée
					'db.pass' => "",			//Mot de passe base de donnée
					'db.name' => "videgrenier"	//Nom base de donnée		
				);
	
?>