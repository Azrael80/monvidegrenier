<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim

		Configuration du site web
	*/
	return array(	'site.name'	=> 'MonVideGrenier',					//Nom du site
					'site.url' 	=> 'http://monvidegrenier.test',		//Url du site
					'mail'		=> 'monvidegrenier80@laposte.net',		//Adresse mail automatique
					'mail.name'	=> 'MonVideGrenier',					//Nom adresse mail
					'mail.activation' => false,							//Envoie d'un mail d'activation pour les comptes
					'announce.need.confirm' => false,					//Les annonces doivent être validées pour apparaître
					'mail.username' => 'monvidegrenier80@laposte.net',	//Nom d'utilisateur serveur SMTP
					'mail.password' => 'Mvg80330',						//Mot de passe serveur SMTP
					'mail.host'	=> 'smtp.laposte.net',					//Hôte serveur SMTP
					'mail.port' => 587,									//Port serveur SMTP
					//Token site mapbox pour la map de localisation
					'mapbox.token' => 'pk.eyJ1IjoiYXpyYWVsOHoiLCJhIjoiY2s4b282aW02MDRjMTNlbXo2ajcxbWZ4MiJ9.LYa86Go6tS2K3MnFLVSyrQ'
				);
	
?>