<?php
	/*
		Redirection vers un lien
		Change le header de la page afin de redirigé l'utilisateur
		Paramètres:
			$url: adresse vers la quelle sera redirigé l'utilisateur
	*/
	function redirect($url)
	{
		header("Location: $url");
	}

	/*
		Vérifier l'extension d'un fichier (doit être une image compatible)
		Retourne un booléen, vrai si l'extension est bonne, faux sinon
		Paramètres:
			$file: fichier à vérifier
	*/
	function checkImageExtension($file) {
		$filename = $file['name'];
		$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
		$file_extension = strtolower($file_extension);

		return in_array($file_extension, ['jpg', 'jpeg', 'bmp', 'png', 'gif']);
	}

	/*
		Sauvegarde l'image reçu dans le format .jpg, et modifie sa taille si besoin
		Paramètres:
			$file: fichier à sauvegarder.
	*/
	function uploadImage($file) {
		global $user;

		$filename = $file['name'];

		$hash = hash("md5", $user->getId().uniqid());

		$location = "./images/announces/$hash.jpg";
		$info = getimagesize($file['tmp_name']);

	  	if ($info['mime'] == 'image/bmp') 
		   	$image = imagecreatefrombmp($file['tmp_name']);
		else if ($info['mime'] == 'image/jpeg' || $info['mime'] == 'image/jpg') 
		   	$image = imagecreatefromjpeg($file['tmp_name']);
		elseif ($info['mime'] == 'image/gif') 
		    $image = imagecreatefromgif($file['tmp_name']);
	  	elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($file['tmp_name']);

		//Changement taille : 
		$w = $info[0];
		$h = $info[1];

		if($w > 1080) {
			$h = $h * (1080 / $w);
			$w = 1080;
		}

		if($h > 1080) {
			$w = $w * (1080 / $h);
			$h = 1080;
		}

		$image = imagescale($image, $w, $h);

	  	imagejpeg($image, $location, 100);
		return $hash;
	}

?>