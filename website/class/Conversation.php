<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Classe pour les conversations : messages
	*/

class Conversation {

	private $user;
	private $secondUser;
	private $messages;
	private $lastDate;
	private $unRead;
	private $lastId;

	/*
		Construit une conversation
		Paramètres:
			$user: Utilisateur principal
			$secondUser: Deuxième utilisateur
			$lastDate: Timestamp du dernier message affiché
			$unRead: Conversation lu ou non
	*/
	function __construct($user, $secondUser, $lastDate = 0, $unRead = false)
	{
		$this->user = $user;
		$this->secondUser = $secondUser;
		$this->lastDate = $lastDate;
		$this->unRead = $unRead;
		$this->lastId = 0;
	}

	//Get
	public function getSecondUser() {
		return $this->secondUser;
	}

	public function getMessages() {
		return $this->messages;
	}

	public function getLastDate() {
		return $this->lastDate;
	}

	public function getUnRead() {
		return $this->unRead;
	}

	public function getLastId() {
		return $this->lastId;
	}

	//Methodes

	/*
		Chargement des messages de la conversation
		Retourne un tableau contenant les messages de la conversation (ainsi que les annonces si certains message sont liés à certaines annonces)
		Change le dernier message affiché (sa date et son identifiant)
	*/
	public function loadMessages() {
		$this->messages = array();

		$request = Config::db()->prepare('SELECT * FROM message WHERE (uId1 = :uid1 AND uId2 = :uid2) OR (uId1 = :uid2 AND uId2 = :uid1) ORDER BY messageId ASC');							
		$request->execute([':uid1' => $this->user->getId(), ':uid2' => $this->secondUser->getId()]);

		while($message = $request->fetch(PDO::FETCH_OBJ)) {
			$this->messages[] = [
				'user' => ($message->uId1 == $this->user->getId() ? $this->user : $this->secondUser),
				'message' => $message,
				'announce' => Announce::findById($message->anId)
			];
		}

		if(count($this->messages) > 0) 
		{
			$this->lastId = $this->messages[count($this->messages) - 1]['message']->messageId;
			$this->lastDate = $this->messages[count($this->messages) - 1]['message']->messageDate;
		}
	}

	/*
		Chargement de tous les messages de la conversation après un identifiant, utile pour les requêtes ajax (conversation en temps réel)
		Retourne un tableau contenant les messages
		Paramètres:
			$lastId: identifiant après le quel chercher les messages
	*/
	public function loadMessagesAfterIdAJAX($lastId) {
		//$lastId = 0;
		$result = array();
		$messages = array();
		$announce = null;

		$request = Config::db()->prepare('SELECT * FROM message WHERE ((uId1 = :uid1 AND uId2 = :uid2) OR (uId1 = :uid2 AND uId2 = :uid1)) AND messageId > :lastId ORDER BY messageId ASC');							
		$request->execute([':uid1' => $this->user->getId(), ':uid2' => $this->secondUser->getId(), ':lastId' => $lastId]);

		while($message = $request->fetch(PDO::FETCH_OBJ)) {
			$message->fullName = ($message->uId1 == $this->user->getId() ? htmlspecialchars($this->user->getFullName()) : htmlspecialchars($this->secondUser->getFullName()));
			$message->messageContent = nl2br(htmlspecialchars($message->messageContent));
			$message->messageDate = date('d/m/Y H:i', $message->messageDate);
			if($message->anId != 0) {
				$announce = Announce::findById($message->anId);
				$message->announceTitle = htmlspecialchars($announce->getTitle());
				$message->announceImage = $announce->getFirstImage();
				$message->announceImgCount = $announce->getImgCount();
				$message->announcePrice = $announce->getPrice();
			}
			$messages[] = $message;
		}

		$result['messages'] = $messages;
		if(count($messages) > 0)
			$result['lastId'] = $messages[count($messages) - 1]->messageId;
		else
			$result['lastId'] = $lastId;

		return $result;
	}

	/*
		Marque tous les messages de la conversation comme vu pour l'utilisateur principal
	*/
	public function view() {
		$request = Config::db()->prepare('UPDATE message SET unRead=0 WHERE uId1 = :uid1 AND uId2 = :uid2');							
		$request->execute([':uid1' => $this->secondUser->getId(), ':uid2' => $this->user->getId()]);
	}

	/*
		Ajoute un message de l'utilisateur principal à la conversation
		Paramètres:
			$message: Contenu du message
			$anId: Identifiant de l'annonce liée (0 si aucune)
	*/
	public function addMessage($message, $anId = 0) {
		$request = Config::db()->prepare('INSERT INTO message (uId1,uId2,messageDate,messageContent,anId) VALUES (?,?,?,?,?)');							
		$request->execute([$this->user->getId(), $this->secondUser->getId(), time(), $message, $anId]);
	}

	//Statique
	/*
		Obtient la liste des conversation d'un utilisateur
		Retourne un tableau de conversations, contenant le nombre total de message à l'indice 0
		Paramètres:
			$user: utilisateur principal de la conversation
			$page: page actuel afin de savoir quelles conversations retourner
	*/
	static public function getConversations($user, $page)
	{
		$result = array();
		$start = ($page - 1) * 10;

		$request = Config::db()->prepare('SELECT SQL_CALC_FOUND_ROWS M.*
			FROM
			    (SELECT
			            IF(uId1 = :id, uId2, uId1) AS interlocuteur,
			            MAX(messageId) AS max_id
			        FROM message
			        WHERE
			            uId1 = :id
			            OR uId2 = :id
			        GROUP BY IF(uId1 = :id, uId2, uId1)
			    ) AS DM
			        INNER JOIN message M
			            ON DM.max_id = M.messageId ORDER BY M.messageDate DESC
		');

		$request->execute([':id' => $user->getId()]);

		//On compte le total de conversations trouvées
		$query = Config::db()->query('SELECT FOUND_ROWS();');
		$result[0] = $query->fetchColumn();
		$result[0] = 1;

		while($conv = $request->fetch(PDO::FETCH_OBJ)) {
			$result[] = new Conversation(	$user, 
											User::findById($conv->uId1 == $user->getId() ? $conv->uId2 : $conv->uId1), 
											$conv->messageDate, 
											($conv->uId2 == $user->getId() && $conv->unRead)
										);
		}

		return $result;
	}
}

?>