<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Classe pour les annonces
	*/

class Announce {
	private $anId;
	private $uId;
	private $catId;
	private $villeId;
	private $anTitle;
	private $anDesc;
	private $anDate;
	private $anPhone;
	private $anPrice;
	private $anValid;
	private $anImages;
	private $anDeleted;

	//Contenu
	private $sender;
	private $category;
	private $city;

	function __construct() {
		$this->anId = -1;
	}

	/*
		Initialise l'instance à partir d'une annonce obtenu dans la base de donnée
		Paramètres:
			$a: Objet PDO obtenu depuis la table announce
	*/
	public function init($a) {
		$this->anId = $a->anId;
		$this->uId = $a->uId;
		$this->catId = $a->catId;
		$this->villeId = $a->ville_id;
		$this->anTitle = $a->anTitle;
		$this->anDesc = $a->anDesc;
		$this->anDate = $a->anDate;
		$this->anPhone = $a->anPhone;
		$this->anPrice = $a->anPrice;
		$this->anValid = $a->anValid;
		$this->anImages = array();
		if($a->anImg1 != NULL)
			$this->anImages[0] = "announces/".$a->anImg1.".jpg";
		else
			$this->anImages[0] = "no_img.png";
		if($a->anImg2 != NULL)
			$this->anImages[1] = "announces/".$a->anImg2.".jpg";
		if($a->anImg3 != NULL)
			$this->anImages[2] = "announces/".$a->anImg3.".jpg";
		$this->anDeleted = $a->anDeleted;

		//Recherche du contenu spécial (Utilisateur, Catégorie et Ville)

		//Utilisateur
		$this->sender = User::findById($this->uId);

		//Catégorie
		$request = Config::db()->prepare('SELECT * FROM category WHERE catId = ?');
		$request->execute([$this->catId]);
		$this->category = $request->fetch(PDO::FETCH_OBJ);

		//Ville
		$request = Config::db()->prepare('SELECT * FROM ville WHERE ville_id = ?');
		$request->execute([$this->villeId]);
		$this->city = $request->fetch(PDO::FETCH_OBJ);
	} 

	//Getters

	public function getId() {
		return $this->anId;
	}

	public function getTitle() {
		return $this->anTitle;
	}

	public function getDesc() {
		return $this->anDesc;
	}

	public function getDate() {
		return $this->anDate;
	}

	public function getPhone() {
		return $this->anPhone;
	}


	public function getPrice() {
		return $this->anPrice;
	}

	public function getValid() {
		return $this->anValid;
	}

	public function getImages() {
		return $this->anImages;
	}

	public function getDeleted() {
		return $this->anDeleted;
	}

	//Compte le nombre d'images qu'a l'annonce, retire l'image par défaut si elle existe
	public function getImgCount() {
		return count($this->anImages) - ($this->anImages[0] == "no_img.png" ? 1 : 0);
	}

	//Retourne la première image
	public function getFirstImage() {
		return $this->anImages[0];
	}

	public function getSender() {
		return $this->sender;
	}

	public function getCity() {
		return $this->city;
	}

	//Setters
	public function setUId($id) {
		$this->uId = $id;
	}

	public function setCatId($id) {
		$this->catId = $id;
	}

	public function setVilleId($id) {
		$this->villeId = $id;
	}

	public function setTitle($title) {
		$this->anTitle = $title;
	}

	public function setDesc($desc) {
		$this->anDesc = $desc;
	}

	public function setPhone($phone) {
		$this->anPhone = $phone;
	}

	public function setPrice($price) {
		$this->anPrice = $price;
	}

	public function setValid($valid) {
		$this->anValid = $valid;
	}

	//Ajoute les images
	public function setImages($images) {
		$max = count($images);
		if($max > 3)
			$max = 3;
		$this->anImages = array();
		for($i = 0; $i < $max; $i++)
			$this->anImages[] = $images[$i];
	}

	public function setDeleted($delet) {
		$this->anDeleted = $delet;
	}

	//Métohodes

	/*
		Recherche des annonces
		Retourne un tableau contenant le nombre total d'annonces trouvées et les annonces de la page
		Paramètres:
			$page: page actuel
			$announcesPerPage: nombre d'annonces par page
			$catId: Identifiant de la catégorie (-1 si pas de catégorie)
			$keywords: mots clés entrés par l'utilisateur (séparés par des espaces)
			$city: objet PDO obtenu à partir de la table ville
			$distance: distance en km de la ville 
			$dpt: objet PDO obtenu à partir de la table departement
			$region: objet PDO obtenu à partir de la table region
			$pMin: prix minimum pour la recherche
			$pMax: prix maximum pour la recherche
			$order: ordre sélectionné 
				t_desc: Annonces plus récentes
				t_asc: Annonces plus anciennes
				p_asc: Prix décroissant
				p_desc: Prix croissant
	*/
	static public function getAnnouncesSearch($page, $announcesPerPage, $catId, $keywords, $city, $distance, $dpt, $region, $pMin, $pMax, $order) {
		//5256000 = 2 mois en secondes (validité max d'une annonce)
		$validDate = time() - 5256000;
		$min = ($page - 1) * $announcesPerPage;
		$categoryCondition = ""; //Condition pour la catégorie
		$keywordsCondition = ""; //Condition des mots clefs
		$keywordsTab = [];	//Paramètres à ajouter à la requete pour les mots clefs
		$cityCondition = ""; //Condition de recherche à partir d'une ville
		$dptCondition = ""; //Condition de recherche à partir d'un département
		$regionCondition = ""; //Condition de recherche à partir des régions
		$priceCondition = ""; //Condition de prix

		//Récupération de la ou des catégories
		if($catId != -1) {
			$request  = Config::db()->prepare('SELECT catId,catParentId FROM category WHERE catId = ?');
			$request->execute([$catId]);
			$category = $request->fetch(PDO::FETCH_OBJ);
			if($category->catParentId == - 1) {
				//Si la catégorie est une catégorie parente, il faut récupérer les annonces qui sont dans
				//toutes les sous-catégories.
				$categoryCondition = "AND catId IN (SELECT catId FROM category WHERE catParentId = $catId)";
			} else {
				//Si la catégorie est une sous-catégorie, il faut récupérer les annonces qui 
				//ont pour catégorie celle demandée
				$categoryCondition = "AND catId = $catId";
			}
		}

		//Création de la condition des mots clefs
		if(strlen($keywords) > 0) {
			$keys = explode(" ", $keywords);
			$i = 0;
			$keywordsTab["key_$i"] = "%$keywords%";

			$keywordsCondition = "AND (anTitle LIKE :key_$i";

			foreach($keys as $k) {
				$i += 1;
			    $keywordsCondition .= " OR anTitle LIKE :key_$i";
			    $keywordsTab["key_$i"] = "%$k%";
			}

			$keywordsCondition .= ')';
		}

		//Recherche à partir d'une ville si une ville est ajoutée
		if($city != null) {
			if($distance <= 0) {
				$cityCondition = 'AND ville_id = '.$city->ville_id;
			} else {
				//conversion de la distance en km en m
				$distance = $distance * 1000;
				
				//Formule pour la distance entre deux points :
				//https://geodesie.ign.fr/contenu/fichiers/Distance_longitude_latitude.pdf

				$cityCondition = 'AND ville_id IN (SELECT v2.ville_id FROM ville v1, ville v2 WHERE v1.ville_id = '.$city->ville_id.' AND (v1.ville_id = v2.ville_id OR 
					((ACOS(SIN(RADIANS(v1.ville_latitude_deg)) * SIN(RADIANS(v2.ville_latitude_deg)) + COS(RADIANS(v1.ville_latitude_deg)) * COS(RADIANS(v2.ville_latitude_deg)) * COS(RADIANS(v1.ville_longitude_deg) - RADIANS(v2.ville_longitude_deg)))) * 6378137) < '.$distance.'
					))';
			}
		}

		//Recherche à partir d'un département si un département est ajouté
		//On doit séléctionner les ids de toutes les villes du département et prendre toutes les annonces
		//qui ont pour ville un de ces identifiants.
		if($dpt != null) {
			$dptCondition = 'AND ville_id IN (SELECT ville_id FROM ville WHERE ville_departement = '.$dpt->departement_code.')';
		}

		//Recherche à partir d'une région si une région est ajoutée
		//On doit séléctionner pour chaque département de la région, les ids de chaques ville de ces départements
		//et prendre toutes les annonces qui ont pour ville un de ces identifiants.
		if($region != null) {
			$regionCondition = 'AND ville_id IN (SELECT ville.ville_id FROM ville, departement WHERE departement.code_region = '.$region->code_region.' AND ville.ville_departement = departement.departement_code)';
		}

		//Formation de la condition pour le prix

		if($pMin > 0 && $pMax > 0) {
			//Les deux prix sont entrés car ils sont plus grand que 0
			$priceCondition = "AND anPrice BETWEEN $pMin AND $pMax";
		} else if($pMin > 0) {
			//Seulement le prix minimum est entré
			$priceCondition = "AND anPrice >= $pMin";
		} else if($pMax > 0) {
			//Seulement le prix maximum est entré
			$priceCondition = "AND anPrice <= $pMax";
		}

		//Transformation de l'ordre pour le format SQL
		switch ($order) {
			case 't_desc':
				$order = 'anDate DESC';
				break;
			case 't_asc':
				$order = 'anDate ASC';
				break;
			case 'p_asc':
				$order = 'anPrice ASC';
				break;
			case 'p_desc':
				$order = 'anPrice DESC';
				break;
		}

		$request = Config::db()->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM announce WHERE anDate > :dateMin AND anValid = 1 AND anDeleted = 0 $categoryCondition $keywordsCondition $cityCondition $dptCondition $regionCondition $priceCondition GROUP BY anId ORDER BY $order LIMIT :min,:aPerPage");
		$request->bindValue('dateMin', $validDate, PDO::PARAM_INT);
		$request->bindValue('min', $min, PDO::PARAM_INT);
		$request->bindValue('aPerPage', $announcesPerPage, PDO::PARAM_INT);	

		//Ajout des mots clefs à la recherche
		foreach ($keywordsTab as $key => $value) {
			$request->bindValue($key, $value, PDO::PARAM_STR);
		}

		$request->execute();

		//On compte le total d'annonces trouvées
		$query = Config::db()->query('SELECT FOUND_ROWS();');

		$result = ['full_count' => $query->fetchColumn(), 'announces' => []];
		$an = null;
		while($a = $request->fetch(PDO::FETCH_OBJ)) {
			//$result['full_count'] = $a->full_count;
			$an = new Announce();
			$an->init($a);
			$result['announces'][] = $an;
		}

		return $result;
	}

	/*
		Retourne toutes les annonces d'un utilisateur
		Paramètres:
			$user: utilisateur ayant posté les annonces recherchées
			$page: page actuel
			$announcesPerPage: nombre d'annonces par page
	*/
	static public function getUserAnnounces($user, $page, $announcesPerPage) {

		$min = ($page - 1) * $announcesPerPage;

		$request = Config::db()->prepare("SELECT * FROM announce WHERE uId = :uId ORDER BY anId DESC LIMIT :min,:aPerPage");

		$request->bindValue('uId', $user->getId(), PDO::PARAM_INT);
		$request->bindValue('min', $min, PDO::PARAM_INT);
		$request->bindValue('aPerPage', $announcesPerPage, PDO::PARAM_INT);	
		$request->execute();

		$result = array();
		$an = null;
		while($a = $request->fetch(PDO::FETCH_OBJ)) {
			$an = new Announce();
			$an->init($a);
			$result[] = $an;
		}

		return $result;
	}

	/*
		Retourne le nombre d'annonces de l'utilisateur
		Paramètres:
			$user: utilisateur ayant posté les annonces recherchées
	*/
	static public function getUserAnnouncesCount($user) {


		$request = Config::db()->prepare("SELECT COUNT(*) FROM announce WHERE uId = :uId");
		$request->bindValue('uId', $user->getId(), PDO::PARAM_INT);
		$request->execute();

		return $request->fetchColumn();
	}

	/*
		Retourne le nombre d'annonces valides (en ligne) qu'a un utilisateur à partir de son identifiant
		Paramètres:
			$uId: identifiant de l'utilisateur ayant posté les annonces
	*/
	static public function getUserAnnouncesValidCount($uId) {
		$validDate = time() - 5256000;
		$request = Config::db()->prepare("SELECT COUNT(*) FROM announce WHERE anDate > ? AND uId = ? AND anValid = 1 AND anDeleted = 0");
		$request->execute([$validDate, $uId]);
		$result = $request->fetchColumn();

		return $result;
	}

	/*
		Retourne les annonces valides (en ligne) qu'a un utilisateur à partir de son identifiant
		Paramètres:
			$uId: identifiant de l'utilisateur ayant posté les annonces
			$page: page actuel
			$announcesPerPage: nombre d'annonces par page
	*/
	static public function getUserAnnouncesValid($uId, $page, $announcesPerPage) {
		//5256000 = 2 mois en secondes (validité max d'une annonce)
		$validDate = time() - 5256000;
		$min = ($page - 1) * $announcesPerPage;

		$request = Config::db()->prepare("SELECT * FROM announce WHERE uId = :uId AND anDate > :dateMin AND anValid = 1 AND anDeleted = 0 ORDER BY anId DESC LIMIT :min,:aPerPage");
		$request->bindValue('uId', $uId, PDO::PARAM_INT);
		$request->bindValue('dateMin', $validDate, PDO::PARAM_INT);
		$request->bindValue('min', $min, PDO::PARAM_INT);
		$request->bindValue('aPerPage', $announcesPerPage, PDO::PARAM_INT);		
		$request->execute();


		$result = array();
		$an = null;
		while($a = $request->fetch(PDO::FETCH_OBJ)) {
			$an = new Announce();
			$an->init($a);
			$result[] = $an;
		}

		return $result;
	}

	/*
		Retourne toutes les annonces qu'a un utilisateur à partir de son identifiant
		Paramètres:
			$uId: identifiant de l'utilisateur ayant posté les annonces
	*/
	static public function findById($id) {
		$request = Config::db()->prepare("SELECT * FROM announce WHERE anId = ?");
		$request->execute([$id]);
		$request = $request->fetch(PDO::FETCH_OBJ);
		if($request != null)
		{
			$a = new Announce();
			$a->init($request);
			return $a;
		}
		return null;
	}

	/*
		Affiche le nom de la catégorie dans la quelle se trouve l'annonce
	*/
	public function showCategoryName() {
		echo $this->category->catName;
	}

	/*
		Affiche le nom de la ville au format Nom, Code Postal de la ville où se trouve l'annonce
	*/
	public function showCityName() {
		echo $this->city->ville_nom_reel.", ".explode('-', $this->city->ville_code_postal)[0];
	}

	/*
		Affiche la date à la quelle l'annonce a été postée au format Aujourd'hui, Hier ou date complète
	*/
	public function showDate() {
		$date = date('d/m/Y', $this->anDate);

	    if($date == date('d/m/Y', time())) {
	      	echo 'Aujourd\'hui, ';
	    } else if($date == date('d/m/Y', time() - (24 * 60 * 60))) {
	      	echo 'Hier, ';
	    } else {
	    	echo "$date ";
	    }
	    //Heure
	    echo date('H:i', $this->anDate);
	}

	/*
		Sauvegarde de l'annonce, en créer une nouvelle si l'identifiant est égal à -1
	*/
	public function save() {
		if($this->anId != -1) { //Mise à jour
			$request = Config::db()->prepare('UPDATE announce SET anDeleted = ?, anValid = ? WHERE anId = ?');
			$request->execute([
				$this->anDeleted ? 1 : 0,
				$this->anValid ? 1 : 0,
				$this->anId
			]);
		} else {
			$request = Config::db()->prepare('INSERT INTO announce(uId, catId, ville_id, anTitle, anDesc, anDate, anPhone, anPrice, anValid, anImg1, anImg2, anImg3) VALUES(:uId,:catId,:ville_id,:anTitle,:anDesc,:anDate,:anPhone,:anPrice,:anValid,:anImg1,:anImg2,:anImg3)');						
			$request->execute([':uId' => $this->uId, ':catId' => $this->catId, ':ville_id' => $this->villeId, ':anTitle' => $this->anTitle, ':anDesc' => $this->anDesc, ':anDate' => time(), ':anPhone' => $this->anPhone, ':anPrice' => $this->anPrice, ':anValid' => $this->anValid, ':anImg1' => (isset($this->anImages[0]) ? $this->anImages[0] : NULL), ':anImg2' => (isset($this->anImages[1]) ? $this->anImages[1] : NULL), ':anImg3' => (isset($this->anImages[2]) ? $this->anImages[2] : NULL)]);
			$this->anId = Config::db()->lastInsertId();
		}
	}
}
?>