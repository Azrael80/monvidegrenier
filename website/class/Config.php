<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Script php qui rassemblera toutes les configurations, accessibles en un objet.
	*/
require_once $_SERVER['DOCUMENT_ROOT']."/config/functions.config.php";

class Config {
	
	private static $config_data = array();
	
	private static $db;
	
	/* Initialise la configuration */
	public static function init()
	{
		self::$config_data = array_merge(	include $_SERVER['DOCUMENT_ROOT']."/config/website.config.php", 
											include $_SERVER['DOCUMENT_ROOT']."/config/database.config.php");

		self::$db = new PDO('mysql:host='.self::get('db.host').';port='.self::get('db.port').';dbname='.self::get('db.name'), self::get('db.user'), self::get('db.pass'), array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

		date_default_timezone_set("Europe/Paris"); //Mise à jour du fuseau horaire
	}

	/*
		Récupère la valeur d'un attribut de configuration
		Paramètre:
			$key: nom de l'attribut demandé
	*/
	public static function get($key) {
		return self::$config_data[$key];
	}
	
	/*
		Retourne la connexion PDO à la base de donnée
	*/
	public static function db() {
		return self::$db;
	}
	
}

Config::init();
?>