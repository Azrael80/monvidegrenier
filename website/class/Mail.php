<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Classe pour les mails

		FONCTIONNE SEULEMENT SI UN SERVEUR SMTP LOCAL EST CONFIGURE !
		Utilise PHPMailer
	*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class Mail {

	private $data;
	private $model;

	private $mailer;
	
	public function __construct() {
		//Initialisation mails
		$this->mailer = new PHPMailer(true);
		//$this->mailer->SMTPDebug = SMTP::DEBUG_SERVER;                     
    	$this->mailer->isSMTP();                                            
    	$this->mailer->Host       = Config::get('mail.host');                    
    	$this->mailer->SMTPAuth   = true;                                   
    	$this->mailer->Username   = Config::get('mail.username');                     
    	$this->mailer->Password   = Config::get('mail.password');                               
    	$this->mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
    	$this->mailer->Port       = Config::get('mail.port');   
    	$this->mailer->setFrom(Config::get('mail'), Config::get('mail.name'));     
    	$this->mailer->CharSet = 'UTF-8';                            
	} 


	//Seters
	public function setReceiver($receiver) {
		$this->mailer->addAddress($receiver);
	}
	
	public function setSubject($subject) {
		$this->mailer->Subject = $subject;
	}

	/*
		Tableau de type clef => valeur permettant de remplacer les textes dans le model du mail
	*/
	public function setData($data) {
		$this->data = $data;
	}

	/*
		Le model est un format html qui va être chargé lors de l'envoie du mail
	*/
	public function setModel($model) {
		$this->model = $model;
	}

	public function setAltBody($content) {
		$this->mailer->AltBody = $content;
	}

	//Sending
	//Envoie du mail avec PHPMailer
	public function send() {
		try {
    		
    		$this->mailer->Body = str_replace(array_keys($this->data), array_values($this->data), file_get_contents($_SERVER['DOCUMENT_ROOT']."/config/mails/".$this->model.".html"));
    		$this->mailer->send();

    	} catch (Exception $e) {
		    echo "Message could not be sent. Mailer Error: {$this->mailer->ErrorInfo}";
		}
	}
}
?>