<?php
	/*
		MonVideGrenier - Projet L2 Info - 2019/2020
		CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim
		
		Classe pour les comptes
	*/

class User {
	
	private $id;
	private $mail;
	private $pass;
	private $name;
	private $forename;
	private $activ;
	private $activCode;
	private $passCode;
	private $registerIp;
	private $lastIp;
	private $registerDate;

	//Autre
	private $unReadPMCount;

	function __construct() {
		$this->id = -1;
	}

	/*
		Initialise l'instance à partir d'un utilisateur obtenu dans la base de donnée
		Paramètres:
			$user: Objet PDO obtenu depuis la table user
	*/
	public function init($user)
	{
		$this->id = $user->uId;
		$this->mail = $user->uMail;
		$this->pass = $user->uPass;
		$this->name = $user->uName;
		$this->forename = $user->uForename;
		$this->activ = $user->uActiv;
		$this->activCode = $user->uActivCode;
		$this->passCode = $user->uPassCode;
		$this->registerIp = $user->uRegisterIp;
		$this->lastIp = $user->uLastIp;
		$this->registerDate = $user->uRegisterDate;

		//Chargement nombre de messages non lu (pour éviter les requêtes vers la BDD à chaque appel de getUnReadPMCount())
		$request = Config::db()->prepare('SELECT COUNT(*) FROM message WHERE uId2 = ? AND unRead=1');						
		$request->execute([$this->id]);

		$this->unReadPMCount = $request->fetchColumn();
	}

	//Getters

	public function getId() {
		return $this->id;
	}

	public function getMail() {
		return $this->mail;
	}

	public function getPass() {
		return $this->pass;
	}
	
	public function getName() {
		return $this->name;
	}

	public function getForename() {
		return $this->forename;
	}

	public function getActivCode() {
		return $this->activCode;
	}

	public function getPassCode() {
		return $this->passCode;
	}

	public function getRegisterDate() {
		return $this->registerDate;
	}

	//Setters

	public function setMail($mail) {
		$this->mail = $mail;
	}

	public function setPass($pass) {
		$this->pass = $pass;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setForename($forename) {
		$this->forename = $forename;
	}

	public function setActiv($activ) {
		$this->activ = $activ;
	}

	public function setActivCode($activCode) {
		$this->activCode = $activCode;
	}

	public function setPassCode($passCode) {
		$this->passCode = $passCode;
	}

	public function setRegisterIp($ip) {
		$this->registerIp = $ip;
	}

	public function setLastIp($ip) {
		$this->lastIp = $ip;
	}

	//Methodes

	/*
		Obtient la liste des conversation de l'utilisateur
		Retourne un tableau de conversations
		Paramètres:
			$page: page actuel afin de savoir quelles conversations retourner
	*/
	public function getConversations($page) {
		return Conversation::getConversations($this, $page);
	}

	/*
		Retourne le nom complet de l'utilisateur au format: Prénom Nom. (raccourcis à la 1er lettre)
	*/
	public function getFullName() {
		return $this->forename.' '.$this->name[0].'.';
	}

	/*
		Retourne le nombre de messages non lus
	*/
	public function getUnReadPMCount() {
		return $this->unReadPMCount;
	}

	/*
		Retourne un tableau contenant toutes les annonces de l'utilisateur (en ligne ou non) 
	*/
	public function getAnnounces() {
		$result = array();
		$a;
		$request = Config::db()->prepare('SELECT * FROM announce WHERE uId = ? ORDER BY anValid ASC, anId DESC');
		$request->execute([$this->id]);

		while($r = $request->fetch(PDO::FETCH_OBJ)) {
			$a = new Announce();
			$a->init($r);
			$result[] = $a;
		}

		return $result;
	}
	


	//Sauvegarde
	/*
		Sauvegarde l'utilisateur, créer un nouvel utilisateur si il n'existe (identifiant -1)
	*/
	public function save() {
		if($this->id != -1) { //Mise à jour
			$request = Config::db()->prepare('UPDATE user SET uMail = :mail, uPass = :pass, uActiv = :activ, uPassCode = :passCode, uLastIp = :ip WHERE uId = :uid');						
			$request->execute(array('mail' => $this->mail, 'pass' => $this->pass, ':activ' => $this->activ ? 1 : 0, ':passCode' => $this->passCode, ':ip' => $this->lastIp, ':uid' => $this->id));
		} else {
			$request = Config::db()->prepare('INSERT INTO user(uMail, uPass, uName, uForename, uActiv, uActivCode, uRegisterIp, uLastIp, uRegisterDate) VALUES(:mail,:pass,:name,:forename,:activ,:activCode,:registerIp,:lastIp,:registerDate);');						
			$request->execute(array('mail' => $this->mail, 'pass' => $this->pass, ':name' => $this->name, ':forename' => $this->forename, ':activ' => $this->activ ? 1 : 0, ':activCode' => $this->activCode, ':registerIp' => $this->registerIp, ':lastIp' => $this->lastIp, ':registerDate' => time()));
			$this->id = Config::db()->lastInsertId();
		}
	}

	//Statique

	/*
		Retrouve un utilisateur à partir d'une adresse e-mail et d'un mot de passe, utilisé pour la connexion
		Retourne une instance de la classe User si l'utilisateur existe sinon null
		Paramètres:
			$mail: adresse e-mail de l'utilisateur
			$pass: mot de passe de l'utilisateur créer avec le hash sha-256
	*/
	public static function connect($mail, $pass)
	{
		$request = Config::db()->prepare('SELECT * FROM user WHERE uMail = :mail AND uPass = :pass');
									
		$request->execute(array(':mail' => $mail, ':pass' => $pass));
		
		$user = $request->fetch(PDO::FETCH_OBJ);

		$realUser = new User();

		if($user != null)
		{
			$realUser->init($user);
			return $realUser;
		}

		return null;
	}

	/*
		Retrouve un utilisateur à partir d'une adresse e-mail
		Retourne une instance de la classe User si l'utilisateur existe sinon null
		Paramètres:
			$mail: adresse e-mail de l'utilisateur
	*/
	public static function findByMail($mail)
	{
		$request = Config::db()->prepare('SELECT * FROM user WHERE uMail = :mail');
									
		$request->execute(array(':mail' => $mail));
		
		$user = $request->fetch(PDO::FETCH_OBJ);

		$realUser = new User();

		if($user != null)
		{
			$realUser->init($user);
			return $realUser;
		}

		return null;
	}

	/*
		Retrouve un utilisateur à partir de son identifiant
		Retourne une instance de la classe User si l'utilisateur existe sinon null
		Paramètres:
			$id: identifiant de l'utilisateur
	*/
	public static function findById($id)
	{
		$request = Config::db()->prepare('SELECT * FROM user WHERE uId = ?');
									
		$request->execute([$id]);
		
		$user = $request->fetch(PDO::FETCH_OBJ);

		$realUser = new User();

		if($user != null)
		{
			$realUser->init($user);
			return $realUser;
		}

		return null;
	}

	/*
		Retrouve un utilisateur à partir d'un code d'activation, utile pour activer un compte
		Retourne une instance de la classe User si l'utilisateur existe sinon null
		Paramètres:
			$code: code d'activation (lien) reçu par mail
	*/
	public static function findByActivCode($code)
	{
		$request = Config::db()->prepare('SELECT * FROM user WHERE uActivCode = ? AND uActiv = 0');
									
		$request->execute([$code]);
		
		$user = $request->fetch(PDO::FETCH_OBJ);

		$realUser = new User();

		if($user != null)
		{
			$realUser->init($user);
			return $realUser;
		}

		return null;
	}

	/*
		Retrouve un utilisateur à partir d'un code de récupération, utile pour le système de "Mot de passe oublié"
		Retourne une instance de la classe User si l'utilisateur existe sinon null
		Paramètres:
			$code: code de récupération (lien) reçu par mail.
	*/
	public static function findByForgot($code)
	{
		$request = Config::db()->prepare('SELECT * FROM user WHERE uPassCode = ? AND uPassCode <> ""');
									
		$request->execute([$code]);
		
		$user = $request->fetch(PDO::FETCH_OBJ);

		$realUser = new User();

		if($user != null)
		{
			$realUser->init($user);
			return $realUser;
		}

		return null;
	}
}
?>