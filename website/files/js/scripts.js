let curOpen;

$(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#send-message-button").click(function() {
	    $.post(data['url'] + "/ajax/messages.ajax.php?action=send",
	    	{
	    		receiver: data.conversation.receiverId,
	    		message: $("#send-message-content").val()
	    	},
	    	function(result) {
	  			switch(result.result) {
	  				case 1:
	  					$("#message-result-box").append(createAlertHtml('danger', result.message));
	  				break;
	  				case 2:
	  					$("#send-message-content").val("");
	  					refreshMessages(data.conversation.receiverId, data.conversation.lastId);
	  				break;
	  			}
			}
		);
    });
    $("#messages-list").animate({ scrollTop: $('#messages-list').prop("scrollHeight")}, 0);

    //Si la conversation existe, on lance la boucle qui actualise les messages.
    if(typeof data.conversation !== 'undefined')
    	messagesLoop();

    //Si la recherche existe, on initialise (le formulaire).
    if(typeof data.search !== 'undefined')
    	initSearch();

    /* 
    	Map: si l'élément existe sur la page, on initialise une map avec leaflet
    */
    if($('#localisation-map').length) {
		var map = L.map('localisation-map').setView([data.announce.lat, data.announce.long], 12);
		L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
		    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		    maxZoom: 18,
		    id: 'mapbox/streets-v11',
		    tileSize: 512,
		    zoomOffset: -1,
		    accessToken: data.accessMapToken
		}).addTo(map);
		var marker = L.marker([data.announce.lat, data.announce.long]).addTo(map);
		marker.bindPopup("<b>" + data.announce.cityname + "</b>")
	}

	/* 
    	Annonce: si l'élément existe sur la page, on initialise un évènement pour les cliques sur le bouton des messages
    */
    if($('#announce-message-button').length) {
    	$('#announce-message-button').click(function() {
    		$('#announce-message-button').hide();
    		$('#announce-message-form').show();

    		$('#announce-message-send').click(function () {
    			$.post(data['url'] + "/ajax/messages.ajax.php?action=send-announce",
				    {
				    	announceId: data.announce.id,
				    	receiver: data.announce.senderId,
				    	message: $("#send-message-content").val()
				    },
				    function(result) {
				    	if(result.result == 2) {
					  		$('#announce-message-form').html("");
					  		$('#announce-message-form').append("<span class=\"text-center\">Message envoyé.</span>");
					  	} else {
					  		$('#announce-message-form').html("");
					  		$('#announce-message-form').append("<span class=\"text-center\">Le message n'a pas pu être envoyé.</span>");
					  	}
				  	}
				);
    		});
    	});
    }
});


function showAndClose(block1, block2, titleBlock, titleText) {
	$(block1).show();
	$(block2).hide();
	$(titleBlock).text(titleText);
}

function createAlertHtml(type, content) {
	var alert = '<div class="alert alert-' + type + ' alert-dismissible fase show col-12">';
	alert += '<button type="button" class="close" data-dismiss="alert">&times;</button>'
	alert += content;
	alert += '</div>';

	return alert;
}

/* Catégories */
function loadCategories(first, catId) {
	var newMessage;
	var c;
	var elem = first ? "#categorySelector" : "#subCategorySelector";
	$.post(data['url'] + "/ajax/announces.ajax.php?action=selectCategory",
	    {
	    	catId: catId
	    },
	    function(result) {
	  		$(elem).html("");
	  		$(elem).append("<option>SELECTIONNER UNE " + (first ? "CATEGORIE" : "SOUS-CATEGORIE") + "</option>");
	  		for(i = 0; i < result.length; i++) {
	  			c = result[i];
	  			if(first) {
	  				$(elem).append("<option onclick=\"loadCategories(false, " + c.catId + ");\">" + c.catName + "</option>");
	  			} else {
	  				$(elem).append("<option value=\"" + c.catId + "\">" + c.catName + "</option>");
	  			}
	  		}
	  	}
	);
}

/* Annonces */
function getAnnounceNumber() {
	//Envoie d'une requête post avec le token et l'id de l'annonce pour récupérer le numéro du proprio
	$.post(data['url'] + "/ajax/announces.ajax.php?action=getPhone",
	    {
	    	id: data.announce.id,
	    	token: data.token 
	    },
	    function(result) {
	    	$('#numberButton').hide();
	    	$('#phone-block').html(result.phone);
	    	$('#phone-block').show();
	  	}
	);
}

/* Messages */
function refreshMessages(receiver, lastId) {
	//Actualise les messages à l'aide d'une requête post
	var newMessage;
	var m;
	$.post(data['url'] + "/ajax/messages.ajax.php?action=get",
	    {
	    	receiver: data.conversation.receiverId,
	    	lastId: data.conversation.lastId
	    },
	    function(result) {
	  		data.conversation.lastId = result.lastId;

	  		for(i = 0; i < result.messages.length; i++) {
	  			m = result.messages[i];
	  			if(m.messageDate != data.conversation.lastDate)
	  			{
	  				newMessage = "<div class=\"row-fluid ml-0 mr-0 font-italic text-center\">";
					newMessage += "<small>" + m.messageDate + "</small>";
					newMessage += "</div>";
					$("#messages-list").append(newMessage);
					data.conversation.lastDate = m.messageDate;
	  			}

	  			//On vérifie si une annonce est liée ou non au message
	  			if(m.anId == 0) {
		  			newMessage = "<div class=\"row\">";
		  			if(m.uId1 != data.accountId)
		  				newMessage += "<div class=\"col-12 col-md-10 mt-1\"><div class=\"message blue\">";
		  			else
						newMessage += "<div class=\"col-12 offset-md-2 col-md-10 mt-1\"><div class=\"message grey float-right\">";
					newMessage += "<span class=\"name\">" + m.fullName + "</span>";
					newMessage += m.messageContent;
		  			newMessage += "</div></div></div>";
		  		} else {
		  			if(m.uId1 != data.accountId) {
		  				newMessage = "<div class=\"row\">";
		  				newMessage += "<div class=\"col-12 col-md-10 mt-1\"><div class=\"message blue\">";
		  				newMessage += "<div class=\"container-fluid p-1\"><div class=\"row\"><div class=\"col-3 text-center\">";
		  				newMessage += "<a class=\"text-dark text-left announce-link\" href=\"" + data.url + "/announce.php?id=" + m.anId + "\" title=\"" + m.announceTitle + "\">";
		  				newMessage += "<img class=\"img-fluid img-thumbnail\" src=\"" + data.url + "/images/" + m.announceImage + "\" />";
		  				newMessage += "<small><span class=\"badge badge-dark font-weight-bold float-left d-none d-md-block mt-1\"><i class=\"fa fa-camera\" style=\"margin-right: 5px;\"></i> " + m.announceImgCount + "</span></small>";
		  				if(m.announcePrice > 0) 
		  					newMessage += "<small><span class=\"badge badge-dark font-weight-bold float-right mt-1\">" + Number.parseFloat(m.announcePrice).toFixed(2) + " <i class=\"fa fa-euro-sign\"></i></span></small>";
		  				else
		  					newMessage += "<small><span class=\"badge badge-dark font-weight-bold float-right mt-1\">GRATUIT</span></small>";
		  				newMessage += "</div>"
		  				newMessage += "<div class=\"col-9 text-left\">";
		  				newMessage += "<span class=\"name\">" + m.fullName + "</span>";
						newMessage += m.messageContent;
						newMessage += "</div></div></div></div></div></div>"
		  			} else {
		  				newMessage = "<div class=\"row\">";
		  				newMessage += "<div class=\"col-12 offset-md-2 col-md-10 mt-1\"><div class=\"message grey float-right\">";
		  				newMessage += "<div class=\"container-fluid p-1\"><div class=\"row\"><div class=\"col-9 text-left\">";
		  				newMessage += "<span class=\"name\">" + m.fullName + "</span>";
						newMessage += m.messageContent;
						newMessage += "</div>"
						newMessage += "<div class=\"col-3 text-center\">";
		  				newMessage += "<a class=\"text-dark text-left announce-link\" href=\"" + data.url + "/announce.php?id=" + m.anId + "\" title=\"" + m.announceTitle + "\">";
		  				newMessage += "<img class=\"img-fluid img-thumbnail\" src=\"" + data.url + "/images/" + m.announceImage + "\" />";
		  				newMessage += "<small><span class=\"badge badge-dark font-weight-bold float-left d-none d-md-block mt-1\"><i class=\"fa fa-camera\" style=\"margin-right: 5px;\"></i> " + m.announceImgCount + "</span></small>";
		  				if(m.announcePrice > 0) 
		  					newMessage += "<small><span class=\"badge badge-dark font-weight-bold float-right mt-1\">" + Number.parseFloat(m.announcePrice).toFixed(2) + " <i class=\"fa fa-euro-sign\"></i></span></small>";
		  				else
		  					newMessage += "<small><span class=\"badge badge-dark font-weight-bold float-right mt-1\">GRATUIT</span></small>";
						newMessage += "</div></div></div></div></div></div>"
		  			}
		  		}

	  			$("#messages-list").append(newMessage);
	  		}

	  		if(result.messages.length > 0) $("#messages-list").animate({ scrollTop: $('#messages-list').prop("scrollHeight")}, 1000);
	  	}
	);
}

//Boucle qui récupère les nouveaux messages pour la conversation toute les 0.5 seconde.
function messagesLoop() {
	setInterval(function(){
  		refreshMessages(data.conversation.receiverId, data.conversation.lastId);
	}, 500);
}

/* Recherche */
//Initialise les champs textes pour les villes, départements et region avec typeahead
function initSearch() {
	//loadSearchCategories(true, -1);

	chooseCategory(data.search.catId);

	var localisations = new Bloodhound({
	  	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('local_name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: data['url'] + "/ajax/announces.ajax.php?action=selectLocalisation&name=-1",
		remote: {
		    url: data['url'] + "/ajax/announces.ajax.php?action=selectLocalisation&name=%QUERY",
		    wildcard: '%QUERY'
		}
	});

	$('#search-localisation-typeahead .typeahead').typeahead(null, {
	  	name: 'localisations',
	  	display: 'local_name',
	  	source: localisations
	});

	//Selection de la localisation
	$('#search-localisation-typeahead').on('typeahead:selected', function (e, item) {
	    chooseLocalisation(item.local_type, item.local_id);
	});
}

function loadSearchCategories(first, catId) {
	var newMessage;
	var c;
	var elem = first ? '#categorySearchSelector' : '#subCategorySearchSelector';
	$.post(data['url'] + "/ajax/announces.ajax.php?action=selectCategory",
	    {
	    	catId: catId
	    },
	    function(result) {
	  		$(elem).html("");
	  		$(elem).append("<option>" + (first ? "CATEGORIE" : "SOUS-CATEGORIE") + "</option>");
	  		//if(!first) $(elem).append("<option onclick=\"loadSearchCategories(true, -1);\">&laquo; RETOUR</option>");
	  		for(i = 0; i < result.length; i++) {
	  			c = result[i];
	  			if(first) {
	  				$(elem).append("<option onclick=\"loadSearchCategories(false, " + c.catId + ");\">" + c.catName + "</option>");
	  			} else {
	  				$(elem).append("<option value=\"" + c.catId + "\">" + c.catName + "</option>");
	  			}
	  		}
	  	}
	);
}

/*
	Fonction qui est appellée pour sélectionner une catégorie
	Paramètres:
		catId: Identifiant de la catégorie
*/
function chooseCategory(catId) {
	$('#categoryModal').modal('hide');
	$('#categoryButton').html($('#catId' + catId).html());
	$('input[name=category]').val(catId);
}

/*
	Fonction qui est appellée lorsque le tri est changé
	Paramètres:
		order: ordre 
*/
function changeOrder(order) {
	$('input[name=order]').val(order);
	$('form#searchForm').submit();
}

/*
	Fonction qui est appellée pour sélectionner une localisation
	S'il s'agit d'une ville local_type 2, on affiche la case pour la distance
	Paramètres:
		catId: Identifiant de la catégorie
*/
function chooseLocalisation(local_type, local_id) {
	$('input[name=local_type]').val(local_type);
	$('input[name=local_id]').val(local_id);

	//Si c'est une ville on affiche la case pour la distance, sinon on la cache
	if(local_type == 2)
		$('#localisation-distance').show();
	else
		$('#localisation-distance').hide();
}