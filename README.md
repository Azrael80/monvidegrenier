# MonVideGrenier

Projet de programmation web L2 INFO 2019/2020

MonVideGrenier permet de créer un site de petites annonces similaire au site 
*leboncoin*.

© 2020 Copyright: CHEKALIL--BOULANGER Mehdy - FERKIOUI Karim - L2INFO Prog. web

Licence: [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

## Fonctionnalités

* Connexion/Inscription (avec ou sans mail de confirmation).
* Possibilité d'ajouter des annonces (valides 2 mois) comprenant:
    1. Un titre.
    2. Une catégorie.
    3. Un numéro de téléphone.
    4. Une ville.
    5. Un prix (0€ = Gratuit).
    6. 0 à 3 photos.
    7. Une description.
* Messagerie privée (s'actualise en temps réel), possibilité de lié une annonce à un message, liste de toutes les conversations.
* Rechercher des annonces et les triées à partir de:
    1. Mot(s) clés (dans le titre).
    2. Une catégorie ou sous-catégorie .
    3. Une localisation (Région, Département ou ville).
    4. Possibilité d'ajouter un rayon si une ville est choisie.
    5. Tranche de prix (minimum, maximum).
* Possibilité d'afficher une annonce particulière (photos, description,...):
    1. Possibilité d'afficher le numéro de téléphone sur l'annonce.
    2. Possibilité d'envoyer un message privé à partir de l'annonce si l'utilisateur est connecté.
    3. Voir la localisation de la ville sur une Map (utilisation de Leaflet et mapbox).
* Affichage du profil d'un utilisateur:
    1. Affichage de son prénom nom.
    2. Possibilité d'envoyer un message privé.
    3. Affichage de toutes ses annonces en ligne.
    4. Affichage de sa date d'inscription.
* Informations concernant son propre compte:
    1. Possibilité de modifier certaines informations
    2. Possibilité de voir ses messages privés
    3. Possibilité de voir toutes ses annonces (en ligne, dépassées et supprimées).
    4. Possibilité d'annuler une vente
    5. Déconnexion
* Système de mot de passe oublié (un serveur mail doit être configuré).

## Pré-requis

Testé avec cette infrastructure:

**Appache version : 2.4.41**\
**PHP version : 7.3.12**\
**MySQL version : 8.0.18**\
ou **MariaDB version : 10.4.10**

## Configuration

### Base de donnée

Afin de configurer le projet il faut en premier lieu créer une base de donnée
et y importer le fichier : *sql/database.sql*


Ensuite il faut modifier le fichier : *website/database.config.php* pour la 
base de donnée
```
'db.host' => "localhost",
'db.port' => "3306",
'db.user' => "root",		
'db.pass' => "",	
'db.name' => "videgrenier"
```

* **db.host:** hôte du serveur MySQL/MariaDB
* **db.port:** port de connexion
* **db.user:** nom d'utilisateur
* **db.pass:** mot de passe
* **db.name:** nom de la base de donnée

### Site web

La configuration du site web est assez basique il n'y a qu'un seul fichier : 
*website/website.config.php*\
Les valeurs par défaut permettent de tester les différentes fonctionnalités
il est quand même plus intéressant de tout paramétrer sois-même.

```
'site.name'	=> 'MonVideGrenier',
'site.url' 	=> 'http://monvidegrenier.test',
'mail'		=> 'monvidegrenier80@laposte.net',
'mail.name'	=> 'MonVideGrenier',
'mail.activation' => false,
'announce.need.confirm' => false,
'mail.username' => 'monvidegrenier80@laposte.net',
'mail.password' => 'Mvg80330',
'mail.host'	=> 'smtp.laposte.net',
'mail.port' => 587,
'mapbox.token' => 'pk.eyJ1IjoiYXpyYWVsOHoiLCJhIjoiY2s4b282aW02MDRjMTNlbXo2ajcxbWZ4MiJ9.LYa86Go6tS2K3MnFLVSyrQ'
```

* **site.name:** nom du site web
* **site.url:** url du site web
* **mail:** adresse mail du site web
* **mail.name:** nom qui figure sur les mails
* **mail.activation:** si activé, l'utilisateur recevra un mail de confirmation
* **mail.username:** nom d'utilisateur pour le serveur de mails
* **mail.password:** mot de passe pour le serveur de mail
* **mail.host:** adresse du serveur smtp
* **mail.port:** port d'accès au serveur smtp
* **mapbox.token:** token [Mapbox](https://www.mapbox.com) pour la localisation
sur les annonces.

### Debug
Il est possible d'activer l'affichage de toutes les erreurs dans le fichier 
*core.php* en modifiant le paramètre:
`define("DEBUG", true);`

### Comptes déjà présent dans la base de donnée
Les comptes de test ont tous pour mot de passe test1234.

## Références
Afin de développer ce petit projet d'étude, nous avons utiliser 
différentes ressources en ligne.

* [Bootstrap](https://getbootstrap.com/) : version 4.4.1
site
* [JQuery](https://jquery.com/) : version 3.4.1
* [PHPMailer](https://github.com/PHPMailer/PHPMailer) : version 6.1
* [Typeahead](https://twitter.github.io/typeahead.js/) : version 0.11.1
* [Fontawesome](https://fontawesome.com/) : version 5.7.0
* [Leaflet](https://leafletjs.com/) : version 1.6.0
* [Base de donnée villes de France](https://sql.sh/736-base-donnees-villes-francaises)
* [Base de donnée départements de France](https://sql.sh/1879-base-donnees-departements-francais)